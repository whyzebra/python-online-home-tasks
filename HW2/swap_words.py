from __future__ import annotations
from collections import OrderedDict


class MyString(str):
    def __new__(cls, *args, **kwargs):
        return str.__new__(cls, *args, **kwargs)

    def get_swap_entries(self, major_sub: str, minor_sub: str) -> dict:
        """
        Returns all entries of two substrings in the initial string as dict,
        where keys are the indexes of first substr char entry in the string,
        values are always equal to substring itself.

        Notice, major_sub entries takes advantage over minor_sub.

        :rtype: dict
        """
        major_len = len(major_sub)
        minor_len = len(minor_sub)
        shortes_len = min(major_len, minor_len)

        entries = OrderedDict()
        cursor = 0
        while cursor < self.__len__() - shortes_len:
            if self[cursor:cursor+major_len] == major_sub:
                entries[cursor] = major_sub
                cursor += major_len
            elif self[cursor:cursor+minor_len] == minor_sub:
                entries[cursor] = minor_sub
                cursor += minor_len
            else:
                cursor += 1

        return entries

    def swap_substrings(self, major_sub: str, minor_sub: str) -> MyString:
        """
        Returns result of swap two substrings as new MyString instance

        All occurrences of :major_sub: will be guaranted
        replaced with :minor_sub: but not vice versa

        :rtype: MyString
        """
        major_len = len(major_sub)
        minor_len = len(minor_sub)

        entries = self.get_swap_entries(major_sub, minor_sub)

        swapped = ''
        cursor = 0
        for entry_index, value in entries.items():
            if value == major_sub:
                swapped += self[cursor:entry_index] + minor_sub
                cursor = entry_index + major_len
            else:
                swapped += self[cursor:entry_index] + major_sub
                cursor = entry_index + minor_len

        if cursor != self.__len__():
            swapped += self[cursor:self.__len__()]

        return MyString(swapped)


if __name__ == "__main__":
    quote = 'The reasonable man adapts himself to the world; \
the unreasonable one persists in trying to adapt the world to himself.'

    quote = MyString(quote)
    quote = quote.swap_substrings('unreasonable', 'reasonable')
    print(quote)
