# **Python Home Task 2**

### **Exercise 1:**
- Return the count of negative numbers in next list `[4, -9, 8, -11, 8]`
- Note: do not use conditions or loops
#### Solution:
Solution is based on usage of Python's generator expression. Generator returns `True` for any negative value and `False` for positive.  
The sum of `True` elements is equal to amount of negative numbers in the given list, because Python treats `True` as `1` and `False` as `0`. 
#### code:
```python
def count_negatives(numbers: list) -> int:
    return sum(num < 0 for num in numbers)

if __name__ == "__main__":
    input_numbers = [4, -9, 8, -11, 8]
    negatives = count_negatives(input_numbers)

    print(f'There are {negatives} negative numbers in list {input_numbers}')
```
#### output:
    D:\python-online-home-tasks\HW2>python count_negatives.py
    There are 2 negative numbers in list [4, -9, 8, -11, 8]

----

### **Exercise 2:**
- You have first 5 best tennis players according API rankings. 
Set the first-place player to last place and vice versa.
- players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka','Karolina Pliskova','Elina Svitolina']
#### Solution:
For this exercise I used Python's capability to swap two variables within one expression.  
In order to use `change_postion(players: list)` with any list, I used `0` and `-1` as indices for _first_ and _last_ elemnets of the list.
#### code:
```python
def change_position(players: list):
    players[0], players[-1] = players[-1], players[0]
    print(players)


if __name__ == "__main__":
    players = [
        'Ashleigh Barty',
        'Simona Halep',
        'Naomi Osaka',
        'Karolina Pliskova',
        'Elina Svitolina'
    ]
    change_position(players)
```
#### output:
    D:\python-online-home-tasks\HW2>python change_position.py
    ['Elina Svitolina', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Ashleigh Barty']

----

### **Exercise 3:**
- Swap words "reasonable" and "unreasonable" in quote:  
_"The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself."_
- Note, do not use `<string>.replace()` or similar

#### Solution:
 To solve this exercise I created my own _class-wrapper_ around default `str`, called `MyString`, and implemeted method `swap_substrings(major_sub, minor_sub)`,  
 which swaps all occurances of `major_sub` substring with `minor_sub` and returns swapped version of `MyString` object.  
 In case of any collisions between substrings, priority is given to `major_sub` substring
#### code:
```python
from __future__ import annotations
from collections import OrderedDict


class MyString(str):
    def __new__(cls, *args, **kwargs):
        return str.__new__(cls, *args, **kwargs)

    def get_swap_entries(self, major_sub: str, minor_sub: str) -> dict:
        """
        Returns all entries of two substrings in the initial string as dict,
        where keys are the indexes of first substr char entry in the string,
        values are always equal to substring itself.

        Notice, major_sub entries takes advantage over minor_sub.

        :rtype: dict
        """
        major_len = len(major_sub)
        minor_len = len(minor_sub)
        shortes_len = min(major_len, minor_len)

        entries = OrderedDict()
        cursor = 0
        while cursor < self.__len__() - shortes_len:
            if self[cursor:cursor+major_len] == major_sub:
                entries[cursor] = major_sub
                cursor += major_len
            elif self[cursor:cursor+minor_len] == minor_sub:
                entries[cursor] = minor_sub
                cursor += minor_len
            else:
                cursor += 1

        return entries

    def swap_substrings(self, major_sub: str, minor_sub: str) -> MyString:
        """
        Returns result of swap two substrings as new MyString instance

        All occurrences of :major_sub: will be guaranted
        replaced with :minor_sub: but not vice versa

        :rtype: MyString
        """
        major_len = len(major_sub)
        minor_len = len(minor_sub)

        entries = self.get_swap_entries(major_sub, minor_sub)

        swapped = ''
        cursor = 0
        for entry_index, value in entries.items():
            if value == major_sub:
                swapped += self[cursor:entry_index] + minor_sub
                cursor = entry_index + major_len
            else:
                swapped += self[cursor:entry_index] + major_sub
                cursor = entry_index + minor_len

        if cursor != self.__len__():
            swapped += self[cursor:self.__len__()]

        return MyString(swapped)


if __name__ == "__main__":
    quote = 'The reasonable man adapts himself to the world; \
the unreasonable one persists in trying to adapt the world to himself.'

    quote = MyString(quote)
    quote = quote.swap_substrings('unreasonable', 'reasonable')
    print(quote)
```
#### output:
    D:\python-online-home-tasks\HW2>python swap_words.py
    The unreasonable man adapts himself to the world; the reasonable one persists in trying to adapt the world to himself.