def count_negatives(numbers: list) -> int:
    return sum(num < 0 for num in numbers)


if __name__ == "__main__":
    input_numbers = [4, -9, 8, -11, 8]
    negatives = count_negatives(input_numbers)

    print(f'There are {negatives} negative numbers in list {input_numbers}')
