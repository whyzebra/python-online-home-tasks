def remember_result(function):
    """Prints last result of function it decorates"""
    last_result = None

    def wrapper(*args, **kwargs):
        nonlocal last_result

        print(f'Last result = {last_result}')
        last_result = function(*args, **kwargs)

    return wrapper


class RememberResult:
    """Prints last result of function it decorates"""
    def __init__(self, function):
        self.__function = function
        self.__last_result = None

    def __call__(self, *args, **kwargs):
        print(f'Last result = {self.__last_result}')
        self.__last_result = self.__function(*args, **kwargs)


# @remember_result
@RememberResult
def sum_list(*args):
    result = ""

    for item in args:
        result += str(item)

    print(f"Current result = '{result}'")

    return result


if __name__ == "__main__":
    sum_list("a", "b")
    print()
    sum_list("abc", "cde")
    print()
    sum_list(3, 4, 5)
