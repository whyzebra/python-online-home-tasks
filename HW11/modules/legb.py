a = "I am global variable!"


def enclosing_function():
    a = "I am variable from enclosed function!"

    def inner_function():

        # a = "I am local variable!"

        # task 1.2.1
        # global a

        # task 1.2.2
        nonlocal a

        print(a)

    # task 1.1
    return inner_function()
