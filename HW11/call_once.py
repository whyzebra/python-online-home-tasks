def call_once(function):
    """Runs decorated function once and cached the result"""
    result_is_cached = False
    cache = None

    def wrapper(*args, **kwargs):
        nonlocal result_is_cached
        nonlocal cache

        if not result_is_cached:
            cache = function(*args, **kwargs)
            result_is_cached = True

        return cache

    return wrapper


class CallOnce:
    """Runs decorated function once and cached the result"""
    def __init__(self, function):
        self.__function = function
        self.__result_is_cached = False
        self.__cache = None

    def __call__(self, *args, **kwargs):
        if not self.__result_is_cached:
            self.__cache = self.__function(*args, **kwargs)
            self.__result_is_cached = True

        return self.__cache

# @call_once
@CallOnce
def sum_of_numbers(a, b):
    return a + b


if __name__ == "__main__":
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(999, 999))
    print(sum_of_numbers(123, 456))
    print(sum_of_numbers(4235255, 73985367))
