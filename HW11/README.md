# **Python Home Task 11**

### **Task 1**
Look through file `modules/legb.py`.
1. Find a way to call `inner_function` without moving it from inside of `enclosed_function`.  
1. Modify **ONE LINE** `ininner_function` to make it print variable **`a`** from global scope.  
1. Modify **ONE LINE** `ininner_function` to make it print variable **`a`** form enclosing function.

*modules/legb.py* :
```python
a = "I am global variable!"


def enclosing_function():
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        print(a)
```
### Solution:
1. To call `inner_function` without moving it from inside of `enclosed_function`,  
I added line `return inner_function`. Now *`modules/legb.py`* hax next structure:
```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        print(a)
    
    return inner_function()
```

2. To make `ininner_function` print variable `a` from global scope,  
I change line `a = "I am local variable!"` to `global a`:
```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        global a
        print(a)
    
    return inner_function()
```

3. To make `ininner_function` print variable `a` form enclosing function,  
I changed line `a = "I am local variable!"` to `nonlocal a`:
### Code:
```python
a = "I am global variable!"


def enclosing_funcion():
    a = "I am variable from enclosed function!"

    def inner_function():
        nonlocal a
        print(a)
    
    return inner_function()
```

----

### **Task 2**
Implement a decorator `@remember_result`, which remembers last result of function it decorates and prints it before next call.
### Solution:
For this task I have coded 2 implementations of decorators:
- decorator `@remember_result`, based on nestetd functions;
- class decorator `@RememberResult`.

In my personal opinion `@RememberResult` decorator realization is much more readable and flexible than nested functions.
### Code:
```python
def remember_result(function):
    """Prints last result of function it decorates"""
    last_result = None

    def wrapper(*args, **kwargs):
        nonlocal last_result

        print(f'Last result = {last_result}')
        last_result = function(*args, **kwargs)

    return wrapper


class RememberResult:
    """Prints last result of function it decorates"""
    def __init__(self, function):
        self.__function = function
        self.__last_result = None

    def __call__(self, *args, **kwargs):
        print(f'Last result = {self.__last_result}')
        self.__last_result = self.__function(*args, **kwargs)


# @remember_result
@RememberResult
def sum_list(*args):
    result = ""

    for item in args:
        result += str(item)

    print(f"Current result = '{result}'")

    return result


if __name__ == "__main__":
    sum_list("a", "b")
    print()
    sum_list("abc", "cde")
    print()
    sum_list(3, 4, 5)
```

### output:
    D:\python-online-home-tasks\HW11>python remember_result.py
    Last result = None
    Current result = 'ab'

    Last result = ab
    Current result = 'abccde'

    Last result = abccde
    Current result = '345'
----

### **Task 3**
Implement a decorator `@call_once`, which runs a function or method once and caches the result.  
All consecutive calls to this function should return cached result no matter the arguments.
### Solution:
For this task I have coded 2 implementations of decorators:
- decorator `@call_once`, based on nestetd functions;
- class decorator `@CallOnce`.

In my personal opinion `@CallOnce` decorator realization is much more readable and flexible than nested functions.
### Code:
```python
def call_once(function):
    """Runs decorated function once and cached the result"""
    result_is_cached = False
    cache = None

    def wrapper(*args, **kwargs):
        nonlocal result_is_cached
        nonlocal cache

        if not result_is_cached:
            cache = function(*args, **kwargs)
            result_is_cached = True

        return cache

    return wrapper


class CallOnce:
    """Runs decorated function once and cached the result"""
    def __init__(self, function):
        self.__function = function
        self.__result_is_cached = False
        self.__cache = None

    def __call__(self, *args, **kwargs):
        if not self.__result_is_cached:
            self.__cache = self.__function(*args, **kwargs)
            self.__result_is_cached = True

        return self.__cache

# @call_once
@CallOnce
def sum_of_numbers(a, b):
    return a + b


if __name__ == "__main__":
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(999, 999))
    print(sum_of_numbers(123, 456))
    print(sum_of_numbers(4235255, 73985367))
```
### output:
    D:\python-online-home-tasks\HW11>python call_once.py
    55
    55
    55
    55
----

### **Task \*4**
1. Run the module `modules/mod_a.py`. Check its result. Explain why does this happen.  
1. Try to change `x` to a `list[1,2,3]`. Explain the result.  
1. Try to change `import` to `from x import *` where `x` - module names. Explain the result.

*modules/mod_a.py*:
```python
import mod_c
import mod_b

print(mod_c.x)
```
*modules/mod_b.py*:
```python
import mod_c

mod_c.x = 5
```
*modules/mod_c.py*:
```python
x = 5
```
### Answer:
1. The result of `mod_a.py` will be `5`; 
1. The result of `mod_a.py` will be `5`;
1. The result of `mod_a.py` will be `5`.

The answer for all cases will be the same, because code inside `mod_b.py` modifies the value, defined in `mod_c.py`.  
However `mod_c.py` is imported first, `mod_b.py` overrides value of `x` variable, defined in `mod_c.py`