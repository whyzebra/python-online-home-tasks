from random import randint


class FibonacciGenerator:
    def __init__(self):
        self.prev, self.cur = 0, 1

    def __next__(self):
        return_value = self.prev
        self.prev, self.cur = self.cur, self.prev+self.cur
        return return_value

    def __iter__(self):
        return self


def run_class_fibonacci_generator(n):
    fibonacci = FibonacciGenerator()
    return [next(fibonacci) for _ in range(n)]


def func_fibonacci_generator(n):
    prev, cur = 0, 1
    yield prev

    for _ in range(n-1):
        yield cur
        prev, cur = cur, prev + cur


def run_func_fibonacci_generator(n):
    return [num for num in func_fibonacci_generator(n)]


def fibonacci_while(n):
    fib = [0, 1]
    stop = 0

    while stop < n-2:
        fib.append(fib[-1] + fib[-2])
        stop += 1

    return fib


def fibonacci_for_loop(n):
    fib = [0, 1]
    for _ in range(n-2):
        fib.append(fib[-1] + fib[-2])

    return fib


def validate_input(n):
    if n.isdigit():
        if 0 <= int(n) <= 999:
            return True

    return False


SOLUTIONS = {
    1: run_class_fibonacci_generator,
    2: run_func_fibonacci_generator,
    3: fibonacci_while,
    4: fibonacci_for_loop,
}


def pick_random_solution(n):
    rand_solution_key = randint(1, len(SOLUTIONS))
    rand_solution_name = SOLUTIONS[rand_solution_key].__name__

    print(f'picked solution: {rand_solution_name}')

    return SOLUTIONS[rand_solution_key](n)


if __name__ == "__main__":
    print('Fibonacci sequence calculator has been started\
(Press `Ctrl+C` to exit)\n')

    try:
        while True:
            n = input('Please, enter the length of sequence: ')

            if not validate_input(n):
                print('Enter positive integer number in range [0; 999]\n')
            else:
                if int(n) < 2:
                    sequence = n
                else:
                    sequence = pick_random_solution(int(n))

                print(f'Fibonacci sequence is {sequence}\n')

    except KeyboardInterrupt:
        print('\n\n---Program has been finished---')
