from typing import List


def matrix_reader(matrix_path) -> List[int]:
    """Generator, yields each line as list of int"""
    for line in open(matrix_path, 'r'):
        matrix_row = line.strip().replace(' ', '').split(',')
        yield list(map(int, matrix_row))


def get_matrix(matrix_path) -> List[int]:
    """
    Returns 2d matrix as list of list[int items] from file :matrix_path:
    """
    return [row for row in matrix_reader(matrix_path)]


def transpose_matrix(matrix) -> List[list]:
    """Returns transposed matrix"""

    # creating empty template of "transposed matrix" with
    # amount of rows equel to matrix amount of columns
    transposed = [[] for _ in matrix[0]]

    # filling "transposed matrix" with appropriate values
    for row in matrix:
        for col, el in enumerate(row):
            transposed[col].append(el)

    return transposed


def save_transposed_matrix(transposed_matrix, transposed_path):
    """Write transposed matrix to file"""
    with open(transposed_path, '+w') as f:
        for row in transposed_matrix:
            f.write(', '.join(map(str, row)) + '\n')


def print_matrix(matrix):
    for row in matrix:
        print(row)


if __name__ == "__main__":
    matrix_path = 'matrix.txt'
    transposed_path = 'transposed.txt'

    matrix = get_matrix(matrix_path)
    transposed = transpose_matrix(matrix)
    save_transposed_matrix(transposed, transposed_path)

    print('Original matrix is:')
    print_matrix(matrix)

    print()

    print('Transposed matrix is:')
    print_matrix(transposed)
