from random import randint


def to_binary(num):
    binary = ''

    while num > 0:
        binary += f'{num % 2}'
        num = num // 2

    return binary[::-1]


def to_binary_generator(num):
    while num > 0:
        yield f'{num % 2}'
        num = num // 2


def run_to_binary_generator(num):
    binary = ''.join(digit for digit in to_binary_generator(num))
    return binary[::-1]


def count_ones_for_loop(binary_num):
    counter = 0

    for el in binary_num:
        if el == '1':
            counter += 1

    return counter


def count_ones_generator_expression(binary_num):
    return sum(True for digit in binary_num if digit == '1')


def count_ones_builtin(binary_num):
    return binary_num.count('1')


def validate_input(n):
    return n.isdigit() and 0 <= int(n)


TO_BINARY_SOLUTIONS = {
    1: to_binary,
    2: run_to_binary_generator,
}


COUNT_ONES_SOLUTIONS = {
    1: count_ones_for_loop,
    2: count_ones_generator_expression,
    3: count_ones_builtin,
}


def pick_random_solution(data, solutions):
    rand_solution_key = randint(1, len(solutions))
    rand_solution_name = solutions[rand_solution_key].__name__

    print(f'picked solution: {rand_solution_name}')

    return solutions[rand_solution_key](data)


if __name__ == "__main__":
    print('Decimal to Binary conventer has been started \
(Press `Ctrl+C` to exit)\n')
    try:
        while True:
            n = input('Please, eneter the number: ')

            if not validate_input(n):
                print('Enter positive integer number\n')
            else:
                binary = pick_random_solution(int(n), TO_BINARY_SOLUTIONS)
                ones_sum = pick_random_solution(binary, COUNT_ONES_SOLUTIONS)
                print(f'Number is {n}, binary representation is {binary}, sum is {ones_sum}\n')

    except KeyboardInterrupt:
        print('\n\n---Program has been finished---')
