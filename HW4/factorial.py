from random import randint


def factorial_recursion(n) -> int:
    if n < 2:
        return 1

    return n * factorial_recursion(n-1)


def factorial_while(num) -> int:
    result = 1

    while num > 1:
        result *= num
        num -= 1

    return result


def factorial_for_loop(num) -> int:
    result = 1

    for n in range(1, num+1):
        result *= n

    return result


def validate_input(n):
    if n.isdigit() and len(n) < 4:
        if 0 <= int(n) <= 999:
            return True

    return False


SOLUTIONS = {
    1: factorial_recursion,
    2: factorial_while,
    3: factorial_for_loop,
}


def pick_random_solution(n):
    rand_solution_key = randint(1, len(SOLUTIONS))
    rand_solution_name = SOLUTIONS[rand_solution_key].__name__

    print(f'picked solution: {rand_solution_name}')

    return SOLUTIONS[rand_solution_key](n)


if __name__ == "__main__":
    print('Factorial calculator has been started (Press `Ctrl+C` to exit)\n')
    try:
        while True:
            n = input('Please, eneter the number: ')

            if not validate_input(n):
                print('Enter positive integer number in range [0; 999]\n')
            else:
                factorial = pick_random_solution(int(n))
                print(f'Factorial of {n} is {factorial}\n')

    except KeyboardInterrupt:
        print('\n\n---Program has been finished---')
