# **Python Home Task 4**

### **Exercise 1 (Transpose a matrix):**
- Write a script that can transpose any matrix

### Solution:
My solution reads matrix of any size from **path/to/matrix/file.txt** using generator function and  
wites transposed matrix to another **path/to/transposed/matrix/file.txt** by given PATH as paramater.  
The algorithm of transposing is based on using 2 *for loops* and constructing new *"transposed matrix"*,  
where each column represents row of initial matrix.
### code:
```python
from typing import List


def matrix_reader(matrix_path) -> List[int]:
    """Generator, yields each line as list of int"""
    for line in open(matrix_path, 'r'):
        matrix_row = line.strip().replace(' ', '').split(',')
        yield list(map(int, matrix_row))


def get_matrix(matrix_path) -> List[int]:
    """
    Returns 2d matrix as list of list[int items] from file :matrix_path:
    """
    return [row for row in matrix_reader(matrix_path)]


def transpose_matrix(matrix) -> List[list]:
    """Returns transposed matrix"""

    # creating empty template of "transposed matrix" with
    # amount of rows equel to matrix amount of columns
    transposed = [[] for _ in matrix[0]]

    # filling "transposed matrix" with appropriate values
    for row in matrix:
        for col, el in enumerate(row):
            transposed[col].append(el)

    return transposed


def save_transposed_matrix(transposed_matrix, transposed_path):
    """Write transposed matrix to file"""
    with open(transposed_path, '+w') as f:
        for row in transposed_matrix:
            f.write(', '.join(map(str, row)) + '\n')


def print_matrix(matrix):
    for row in matrix:
        print(row)


if __name__ == "__main__":
    matrix_path = 'matrix.txt'
    transposed_path = 'transposed.txt'

    matrix = get_matrix(matrix_path)
    transposed = transpose_matrix(matrix)
    save_transposed_matrix(transposed, transposed_path)

    print('Original matrix is:')
    print_matrix(matrix)

    print()

    print('Transposed matrix is:')
    print_matrix(transposed)
```
### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW4$ python3 transpose_matrix.py 
    Original matrix is:
    [1, 2]
    [3, 4]
    [5, 6]

    Transposed matrix is:
    [1, 3, 5]
    [2, 4, 6]

----

### **Exercise 2 (Factorial):**
- Calculate the factorial for positiva number ***n***  
- Note: the factorial of a positive integer ***n*** is the product of all positive integers less than or equel to ***n***:  
<div align="center">
    <h3>
        <p><i>n! = n • (n-1) • (n-2) • (n-3) · · · · · 3 • 2 • 1</i></p>
    </h3>
</div>

### Solution:
I have implemented 3 different methods to calculate factorial:
1. recursion
1. while loop
1. for loop  

Driver code randomly picks any of these methods and calculates factorial according to user input.
### code:
```python
from random import randint


def factorial_recursion(n) -> int:
    if n < 2:
        return 1

    return n * factorial_recursion(n-1)


def factorial_while(num) -> int:
    result = 1

    while num > 1:
        result *= num
        num -= 1

    return result


def factorial_for_loop(num) -> int:
    result = 1

    for n in range(1, num+1):
        result *= n

    return result


def validate_input(n):
    if n.isdigit() and len(n) < 4:
        if 0 <= int(n) <= 999:
            return True

    return False


SOLUTIONS = {
    1: factorial_recursion,
    2: factorial_while,
    3: factorial_for_loop,
}


def pick_random_solution(n):
    rand_solution_key = randint(1, len(SOLUTIONS))
    rand_solution_name = SOLUTIONS[rand_solution_key].__name__

    print(f'picked solution: {rand_solution_name}')

    return SOLUTIONS[rand_solution_key](n)


if __name__ == "__main__":
    print('Factorial calculator has been started (Press `Ctrl+C` to exit)\n')
    try:
        while True:
            n = input('Please, eneter the number: ')

            if not validate_input(n):
                print('Enter positive integer number in range [0; 999]\n')
            else:
                factorial = pick_random_solution(int(n))
                print(f'Factorial of {n} is {factorial}\n')

    except KeyboardInterrupt:
        print('\n\n---Program has been finished---')
```
### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW4$ python3 factorial.py 
    Factorial calculator has been started (Press `Ctrl+C` to exit)

    Please, eneter the number: 5
    picked solution: factorial_recursion
    Factorial of 5 is 120

    Please, eneter the number: 23
    picked solution: factorial_for_loop
    Factorial of 23 is 25852016738884976640000

    Please, eneter the number: 3
    picked solution: factorial_while
    Factorial of 3 is 6

    Please, eneter the number: abcd
    Enter positive integer number in range [0; 999]

    Please, eneter the number: ^C

    ---Program has been finished--- 

----

### **Exercise 3 (Fibonacci sequence):**
- For a positive integer ***n*** calculate the result value, which is equal to the sum of the first ***n*** Fibonacci numbers.  
- Note: Fibonacci numbers are a series of numbers in which each next number is equal to the sum of the two preceding ones:  

<div align="center">
    <h3>
        <p><i>0, 1, 1, 2, 3, 5, 8, 13 . . .</i></p>
        <p><i>(F(0) = 0, F(1) = F(2) =1), then F(n) = F(n-1) + F(n-2) for n > 2</i></p>
    </h3>
</div>  

### Solution:
I have implemented 4 different methods of calculating Fibonacci sequence, using:
- *class FibonacciGenerator*
1. generator function *def func_fibonacci_generator(n)*
1. while loop *def fibonacci_while(n)*
1. for loop  *def fibonacci_for_loop(n)*

Driver code randomly picks any of these methods and calculates **Fibonacci sequence** according to user input.
### code:
```python
from random import randint


class FibonacciGenerator:
    def __init__(self):
        self.prev, self.cur = 0, 1

    def __next__(self):
        return_value = self.prev
        self.prev, self.cur = self.cur, self.prev+self.cur
        return return_value

    def __iter__(self):
        return self


def run_class_fibonacci_generator(n):
    fibonacci = FibonacciGenerator()
    return [next(fibonacci) for _ in range(n)]


def func_fibonacci_generator(n):
    prev, cur = 0, 1
    yield prev

    for _ in range(n-1):
        yield cur
        prev, cur = cur, prev + cur


def run_func_fibonacci_generator(n):
    return [num for num in func_fibonacci_generator(n)]


def fibonacci_while(n):
    fib = [0, 1]
    stop = 0

    while stop < n-2:
        fib.append(fib[-1] + fib[-2])
        stop += 1

    return fib


def fibonacci_for_loop(n):
    fib = [0, 1]
    for _ in range(n-2):
        fib.append(fib[-1] + fib[-2])

    return fib


def validate_input(n):
    if n.isdigit():
        if 0 <= int(n) <= 999:
            return True

    return False


SOLUTIONS = {
    1: run_class_fibonacci_generator,
    2: run_func_fibonacci_generator,
    3: fibonacci_while,
    4: fibonacci_for_loop,
}


def pick_random_solution(n):
    rand_solution_key = randint(1, len(SOLUTIONS))
    rand_solution_name = SOLUTIONS[rand_solution_key].__name__

    print(f'picked solution: {rand_solution_name}')

    return SOLUTIONS[rand_solution_key](n)


if __name__ == "__main__":
    print('Fibonacci sequence calculator has been started\
(Press `Ctrl+C` to exit)\n')

    try:
        while True:
            n = input('Please, enter the length of sequence: ')

            if not validate_input(n):
                print('Enter positive integer number in range [0; 999]\n')
            else:
                if int(n) < 2:
                    sequence = n
                else:
                    sequence = pick_random_solution(int(n))

                print(f'Fibonacci sequence is {sequence}\n')

    except KeyboardInterrupt:
        print('\n\n---Program has been finished---')

```
### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW4$ python3 fibonacci.py 
    Fibonacci sequence calculator has been started(Press `Ctrl+C` to exit)

    Please, enter the length of sequence: 4
    picked solution: run_func_fibonacci_generator
    Fibonacci sequence is [0, 1, 1, 2]

    Please, enter the length of sequence: 6
    picked solution: fibonacci_while
    Fibonacci sequence is [0, 1, 1, 2, 3, 5]

    Please, enter the length of sequence: 8
    picked solution: run_func_fibonacci_generator
    Fibonacci sequence is [0, 1, 1, 2, 3, 5, 8, 13]

    Please, enter the length of sequence: 5
    picked solution: fibonacci_for_loop
    Fibonacci sequence is [0, 1, 1, 2, 3]

    Please, enter the length of sequence: abc
    Enter positive integer number in range [0; 999]

    Please, enter the length of sequence: ^C

    ---Program has been finished---

----

### **Exercise 4 (Binary representation):**
- For a positive integer ***n*** calculate the result value, which is equal to the sum of the "1" in the binary representation of ***n***
### Solution:
I have implemented two approachs of transforming positive integer to binary, using:
1. while loop: *def to_binary(num)*
1. generator function: *def to_binary_generator(num)*

For calculating amount of "1" in binary representation, I coded 3 solutions, based on:
1. for loop: *def count_ones_for_loop(binary_num)*
1. generator expression: *def count_ones_generator_expression(binary_num)*
1. built-in *str.count()*

Driver code randomly picks any of these solutions, calculates binary representation and  
amount of "1" in it, according to user input.
### code:
```python
from random import randint


def to_binary(num):
    binary = ''

    while num > 0:
        binary += f'{num % 2}'
        num = num // 2

    return binary[::-1]


def to_binary_generator(num):
    while num > 0:
        yield f'{num % 2}'
        num = num // 2


def run_to_binary_generator(num):
    binary = ''.join(digit for digit in to_binary_generator(num))
    return binary[::-1]


def count_ones_for_loop(binary_num):
    counter = 0

    for el in binary_num:
        if el == '1':
            counter += 1

    return counter


def count_ones_generator_expression(binary_num):
    return sum(True for digit in binary_num if digit == '1')


def count_ones_builtin(binary_num):
    return binary_num.count('1')


def validate_input(n):
    return n.isdigit() and 0 <= int(n)


TO_BINARY_SOLUTIONS = {
    1: to_binary,
    2: run_to_binary_generator,
}


COUNT_ONES_SOLUTIONS = {
    1: count_ones_for_loop,
    2: count_ones_generator_expression,
    3: count_ones_builtin,
}


def pick_random_solution(data, solutions):
    rand_solution_key = randint(1, len(solutions))
    rand_solution_name = solutions[rand_solution_key].__name__

    print(f'picked solution: {rand_solution_name}')

    return solutions[rand_solution_key](data)


if __name__ == "__main__":
    print('Decimal to Binary conventer has been started \
(Press `Ctrl+C` to exit)\n')
    try:
        while True:
            n = input('Please, eneter the number: ')

            if not validate_input(n):
                print('Enter positive integer number\n')
            else:
                binary = pick_random_solution(int(n), TO_BINARY_SOLUTIONS)
                ones_sum = pick_random_solution(binary, COUNT_ONES_SOLUTIONS)
                print(f'Number is {n}, binary representation is {binary}, sum is {ones_sum}\n')

    except KeyboardInterrupt:
        print('\n\n---Program has been finished---')
```
### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW4$ python3 binary_representation.py 
    Decimal to Binary conventer has been started (Press `Ctrl+C` to exit)

    Please, eneter the number: 12
    picked solution: run_to_binary_generator
    picked solution: count_ones_generator_expression
    Number is 12, binary representation is 1100, sum is 2

    Please, eneter the number: 76
    picked solution: to_binary
    picked solution: count_ones_for_loop
    Number is 76, binary representation is 1001100, sum is 3

    Please, eneter the number: abc
    Enter positive integer number

    Please, eneter the number: 33
    picked solution: run_to_binary_generator
    picked solution: count_ones_builtin
    Number is 33, binary representation is 100001, sum is 2

    Please, eneter the number: ^C

    ---Program has been finished---