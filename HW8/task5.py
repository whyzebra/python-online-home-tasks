try:
    if '1' != 1:
        raise 'Error'
    else:
        print('Error has not occured')
except 'Error':
    print('Error has occured')


# D:\python-online-home-tasks\HW8>python task5.py
# Traceback (most recent call last):
#   File "D:\python-online-home-tasks\HW8\task5.py", line 3, in <module>
#     raise 'Error'
# TypeError: exceptions must derive from BaseException

# During handling of the above exception, another exception occurred:

# Traceback (most recent call last):
#   File "D:\python-online-home-tasks\HW8\task5.py", line 6, in <module>
#     except 'Error':
# TypeError: catching classes that do not inherit from BaseException is not
# allowed
