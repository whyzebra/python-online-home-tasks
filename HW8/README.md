# **Python Home Task 8**

### **Task 1:**
```python
a = int(input("Enter the number: "))
b = 7

assert a > b, "Not enough"
```
### output:
    D:\python-online-home-tasks\HW8>python task1.py
    Enter the number: 10

    D:\python-online-home-tasks\HW8>python task1.py
    Enter the number: 1
    Traceback (most recent call last):
    File "D:\python-online-home-tasks\HW8\task1.py", line 4, in <module>
        assert a > b, "Not enough"
    AssertionError: Not enough

    D:\python-online-home-tasks\HW8>python task1.py
    Enter the number: ValueError:(
    Traceback (most recent call last):
    File "D:\python-online-home-tasks\HW8\task1.py", line 1, in <module>
        a = int(input("Enter the number: "))
    ValueError: invalid literal for int() with base 10: 'ValueError:('
----

### **Task 2:**
```python
def foo():
    try:
        bar(x, 4)
    finally:
        print('after bar')
    print('or this after bar?')


foo()
```
### output:
    D:\python-online-home-tasks\HW8>python task2.py
    after bar
    Traceback (most recent call last):
    File "D:\python-online-home-tasks\HW8\task2.py", line 9, in <module>
        foo()
    File "D:\python-online-home-tasks\HW8\task2.py", line 3, in foo
        bar(x, 4)
    NameError: name 'bar' is not defined
----

### **Task 3:**
```python
def baz():
    try:
        return 1
        with open('/tmp/logs.txt') as file:
            print(file.read())
            return
    finally:
        return 2


result = baz()
print(result)
```
### output:
    D:\python-online-home-tasks\HW8>python task3.py
    2
----

### **Task 4:**
```python
def baz():
    try:
        return
    finally:
        print(2)
    else:
        print(3)


foo()
```
### output:
    D:\python-online-home-tasks\HW8>python task4.py
    File "D:\python-online-home-tasks\HW8\task4.py", line 6
        else:
        ^
    SyntaxError: invalid syntax
----

### **Task 5:**
```python
try:
    if '1' != 1:
        raise 'Error'
    else:
        print('Error has not occured')
except 'Error':
    print('Error has occured')
```
### output:
    D:\python-online-home-tasks\HW8>python task5.py
    Traceback (most recent call last):
    File "D:\python-online-home-tasks\HW8\task5.py", line 3, in <module>
        raise 'Error'
    TypeError: exceptions must derive from BaseException

    During handling of the above exception, another exception occurred:

    Traceback (most recent call last):
    File "D:\python-online-home-tasks\HW8\task5.py", line 6, in <module>
        except 'Error':
    TypeError: catching classes that do not inherit from BaseException is not allowed
----

### **Task 6:**
```python
flag = False
while not flag:
    try:
        filename = input('Enter filename: ')
        file = open(filename, 'r')
    except:
        print('Input file not found')
```
### output:
    D:\python-online-home-tasks\HW8>python task6.py
    Enter filename: file
    Input file not found
    Enter filename: task6.py
    Enter filename: task26.py
    Input file not found
