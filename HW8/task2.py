def foo():
    try:
        bar(x, 4)
    finally:
        print('after bar')
    print('or this after bar?')


foo()

# D:\python-online-home-tasks\HW8>python task2.py
# after bar
# Traceback (most recent call last):
#   File "D:\python-online-home-tasks\HW8\task2.py", line 9, in <module>
#     foo()
#   File "D:\python-online-home-tasks\HW8\task2.py", line 3, in foo
#     bar(x, 4)
# NameError: name 'bar' is not defined
