import collections


def my_counter(data: str) -> dict:
    """
    Return dict, where 'keys' are chars in string and 'values' are number of occurances
    """
    counter = collections.defaultdict(int)

    for char in data:
        counter[char] += 1

    return dict(counter)


if __name__ == "__main__":
    data = 'StriAsampleee, hello  !'
    print(f'string: {data}')

    print('my_counter\t    ->', my_counter(data))
    print('collections.Counter ->', dict(collections.Counter(data)))
