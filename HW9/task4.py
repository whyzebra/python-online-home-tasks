import string
import collections


ASCII_LOWER_SET = set(string.ascii_lowercase)


def get_common_chars(*data):
    """Return characters that appear in all strings"""
    common_chars = set(data[0])

    for line in data[1:]:
        common_chars = set(line).intersection(common_chars)

    return common_chars


def get_used_chars(*data):
    """Return characters that appear in at least one string"""
    used_chars = set()

    for line in data:
        used_chars = used_chars.union(set(line))

    return used_chars


def get_unused_chars(*data):
    """Return characters of alphabet, that were not used in any string"""
    return ASCII_LOWER_SET.difference(get_used_chars(*data))


def get_chars_seen_twice(*data):
    """Return characters that appear at least in two strings"""
    fake_counter = collections.defaultdict(int)
    result = set()

    for line in data:
        for char in set(line) - result:
            fake_counter[char] += 1

            if fake_counter[char] == 2:
                result.add(char)

    return result


if __name__ == "__main__":
    print('Task 4')
    test_strings = ["helloJZ", "world", "pythonUZ"]
    print('list of strings:', test_strings)
    print('get_common_chars()\t->', get_common_chars(*test_strings))
    print('get_used_chars()\t->', get_used_chars(*test_strings))
    print('get_unused_chars()\t->', get_unused_chars(*test_strings))
    print('get_chars_seen_twice()\t->', get_chars_seen_twice(*test_strings))
