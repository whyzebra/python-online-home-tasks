def replace_quotes(string: str) -> str:
    """Replace all occurance of double quotes " to single quote ' and vice versa"""
    new_string = ""

    for char in string:
        if char == "'":
            new_string += "\""
        elif char == "\"":
            new_string += "'"
        else:
            new_string += char

    return new_string


if __name__ == "__main__":
    initial_string = "All 'single quoted' phrases are now \"double quoted\" and \"vice\" 'versa'"
    new_string = replace_quotes(initial_string)

    print('inital string: ', initial_string)
    print('swaped quotes: ', new_string)
