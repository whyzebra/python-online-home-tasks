# **Python Home Task 9**

### **Task 1**
Implement a function which receives a string and replaces all " symbols with ' and vise versa.
### Solution:
I've chosen the most straightforward approach for this task: 
iterate over each character in string and  
construct new one with `'` replaced to `"` and vice versa.

```python
def replace_quotes(string: str) -> str:
    """Replace all occurance of double quotes " to single quote ' and vice versa"""
    new_string = ""

    for char in string:
        if char == "'":
            new_string += "\""
        elif char == "\"":
            new_string += "'"
        else:
            new_string += char

    return new_string


if __name__ == "__main__":
    initial_string = "All 'single quoted' phrases are now \"double quoted\" and \"vice\" 'versa'"
    new_string = replace_quotes(initial_string)

    print('inital string: ', initial_string)
    print('swaped quotes: ', new_string)
```
### output:
    D:\python-online-home-tasks\HW9>python replace_quotes.py
    inital string:  All 'single quoted' phrases are now "double quoted" and "vice" 'versa'
    swaped quotes:  All "single quoted" phrases are now 'double quoted' and 'vice' "versa"
----

### **Task 2**
Write a function that check whether a string is a **palindrome** or not. To check your implementation you can use strings from here (https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).
### Solution:
I separated this task on two stages:
1. **Filter** string, to get only **alphabetic** and **numeric** values without whitespaces and any other characters;
1. **Compare** filtered string to itself in **reverse**, in case of similarity - string is palindrome.

Also, I've added some test cases, represented as a `dcit`, where test data is `key` and `value` is the expected answer.
```python
def is_alnum(char: str) -> bool:
    return char.isalnum()


def is_palindrome(string: str) -> bool:
    raw_data = "".join(filter(is_alnum, string.lower()))

    # this is 65% faster than commented code below
    return raw_data == raw_data[::-1]

    # for start, end in zip(raw_data, raw_data[::-1]):
    #     if start != end:
    #         return False

    # return True


if __name__ == "__main__":
    test_set = {
        '123 321': True,
        'abCba': True,
        'Mr. Owl ate my metal worm': True,
        'Was it a car or a cat I saw?': True,

        '123 ab 321': False,
        'Hannah + Otto': False,
        'Mr. Owl ate my letal worm': False
    }

    for test, expected in test_set.items():
        test_status = 'FAILED'
        result = is_palindrome(test)

        if result == expected:
            test_status = 'PASSED'

        answer = 'IS NOT palindrom'
        if result:
            answer = 'IS palindrom'

        print(f'"{test}" {answer} | {test_status}')
```
### output:
    D:\python-online-home-tasks\HW9>python palindrome.py
    "123 321" IS palindrom | PASSED
    "abCba" IS palindrom | PASSED
    "Mr. Owl ate my metal worm" IS palindrom | PASSED
    "Was it a car or a cat I saw?" IS palindrom | PASSED
    "123 ab 321" IS NOT palindrom | PASSED
    "Hannah + Otto" IS NOT palindrom | PASSED
    "Mr. Owl ate my letal worm" IS NOT palindrom | PASSED
----

### **Task 3**
Implement a function `get_shortest_word(s: str) -> str` which returns the shortest word in the given string.  
The word can contain any symbols except whitespaces ( , \n, \t and so on). If there are multiple shortest words  
in the string with a same length return the word that occurs first. Usage of any split functions is forbidden.  

Example:
```python
>>> get_shortest_word('Python is simple and effective!')
'is'
>>> get_shortest_word('Any pythonistalike namespaces a lot, a? O')
'a'
```
### Solution:
```python
def get_shortest_word(string: str) -> str:
    sub_str = ""
    shortest_word = string

    for char in string:
        if char != " ":
            sub_str += char
        else:
            if len(sub_str) < len(shortest_word):
                shortest_word = sub_str

            sub_str = ""

    return shortest_word


if __name__ == "__main__":
    string = '"Python is simple and effective!"'
    print(f'{string} -> shortest word is "{get_shortest_word(string)}"')

    string = '"Any pythonistalike namespaces a lot, a? O"'
    print(f'{string} -> shortest word is "{get_shortest_word(string)}"')
```
### output:
    D:\python-online-home-tasks\HW9>python get_shortest_word.py
    "Python is simple and effective!" -> shortest word is "is"
    "Any pythonistalike namespaces a lot, a? O" -> shortest word is "a"
----

### **Task 4**
Implement a bunch of functions which receive a changeable number of strings and return next parameters:
1. characters that appear in all strings  
1. characters that appear in at least one string  
1. characters that appear at least in two strings  
1. characters of alphabet, that were not used in any string

Note: use `string.ascii_lowercase` for list of alphabet letters
```python
pythontest_strings= ["hello", "world", "python", ]
print(test_1_1(*strings))>>> {'o'}
print(test_1_2(*strings))>>> {'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(test_1_3(*strings))>>> {'h', 'l', 'o'}
print(test_1_4(*strings))>>> {'a', 'b', 'c', 'f', 'g', 'i', 'j', 'k', 'm', 'q', 's', 'u', 'v', 'x', 'z'}
```

### Solution:
```python
import string
import collections


ASCII_LOWER_SET = set(string.ascii_lowercase)


def get_common_chars(*data):
    """Return characters that appear in all strings"""
    common_chars = set(data[0])

    for line in data[1:]:
        common_chars = set(line).intersection(common_chars)

    return common_chars


def get_used_chars(*data):
    """Return characters that appear in at least one string"""
    used_chars = set()

    for line in data:
        used_chars = used_chars.union(set(line))

    return used_chars


def get_unused_chars(*data):
    """Return characters of alphabet, that were not used in any string"""
    return ASCII_LOWER_SET.difference(get_used_chars(*data))


def get_chars_seen_twice(*data):
    """Return characters that appear at least in two strings"""
    fake_counter = collections.defaultdict(int)
    result = set()

    for line in data:
        for char in set(line) - result:
            fake_counter[char] += 1

            if fake_counter[char] == 2:
                result.add(char)

    return result


if __name__ == "__main__":
    print('Task 4')
    test_strings = ["helloJZ", "world", "pythonUZ"]
    print('list of strings:', test_strings)
    print('get_common_chars()\t->', get_common_chars(*test_strings))
    print('get_used_chars()\t->', get_used_chars(*test_strings))
    print('get_unused_chars()\t->', get_unused_chars(*test_strings))
    print('get_chars_seen_twice()\t->', get_chars_seen_twice(*test_strings))
```
### output:
    D:\python-online-home-tasks\HW9>python task4.py
    Task 4
    list of strings: ['helloJZ', 'world', 'pythonUZ']
    get_common_chars()      -> {'o'}
    get_used_chars()        -> {'o', 'p', 'U', 'h', 'w', 't', 'e', 'J', 'y', 'r', 'Z', 'l', 'n', 'd'}
    get_unused_chars()      -> {'f', 'z', 'j', 'k', 'a', 'x', 'q', 'v', 'u', 'g', 'i', 's', 'b', 'c', 'm'}
    get_chars_seen_twice()  -> {'o', 'Z', 'l', 'h'}
----

### **Task 5**
Implement a function, that takes string as an argument and returns a dictionary, that contains letters of  
given string as keys and a number of their occurrence as values.
```python
pythonprint(count_letters("stringsample"))
>>> {'s': 2, 't': 1, 'r': 1, 'i': 1,'n': 1, 'g': 1, 'a': 1, 'm': 1,'p': 1, 'l': 1, 'e': 1}
```
### Solution:
```python
import collections


def my_counter(data: str) -> dict:
    """
    Return dict, where 'keys' are chars in string and 'values' are number of occurances
    """
    counter = collections.defaultdict(int)

    for char in data:
        counter[char] += 1

    return dict(counter)


if __name__ == "__main__":
    data = 'StriAsampleee, hello  !'
    print(f'string: {data}')

    print('my_counter\t    ->', my_counter(data))
    print('collections.Counter ->', dict(collections.Counter(data)))
```
### output:
    D:\python-online-home-tasks\HW9>python counter.py
    string: stringsampleee
    my_counter          -> {'s': 2, 't': 1, 'r': 1, 'i': 1, 'n': 1, 'g': 1, 'a': 1, 'm': 1, 'p': 1, 'l': 1, 'e': 3}
    collections.Counter -> {'s': 2, 't': 1, 'r': 1, 'i': 1, 'n': 1, 'g': 1, 'a': 1, 'm': 1, 'p': 1, 'l': 1, 'e': 3}