def is_alnum(char: str) -> bool:
    return char.isalnum()


def is_palindrome(string: str) -> bool:
    raw_data = "".join(filter(is_alnum, string.lower()))

    # this is 65% faster than commented code below
    return raw_data == raw_data[::-1]

    # for start, end in zip(raw_data, raw_data[::-1]):
    #     if start != end:
    #         return False

    # return True


if __name__ == "__main__":
    test_set = {
        '123 321': True,
        'abCba': True,
        'Mr. Owl ate my metal worm': True,
        'Was it a car or a cat I saw?': True,

        '123 ab 321': False,
        'Hannah + Otto': False,
        'Mr. Owl ate my letal worm': False
    }

    for test, expected in test_set.items():
        test_status = 'FAILED'
        result = is_palindrome(test)

        if result == expected:
            test_status = 'PASSED'

        answer = 'IS NOT palindrom'
        if result:
            answer = 'IS palindrom'

        print(f'"{test}" {answer} | {test_status}')
