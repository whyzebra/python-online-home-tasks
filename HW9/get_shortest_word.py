def get_shortest_word(string: str) -> str:
    sub_str = ""
    shortest_word = string

    for char in string:
        if char != " ":
            sub_str += char
        else:
            if len(sub_str) < len(shortest_word):
                shortest_word = sub_str

            sub_str = ""

    return shortest_word


if __name__ == "__main__":
    string = '"Python is simple and effective!"'
    print(f'{string} -> shortest word is "{get_shortest_word(string)}"')

    string = '"Any pythonistalike namespaces a lot, a? O"'
    print(f'{string} -> shortest word is "{get_shortest_word(string)}"')
