def fizz_buzz():
    while True:
        try:
            num = int(input('Enter number: '))
        except ValueError:
            print('Please, enter only int numbers in range [1, 100]\n')
            continue

        if not 0 < num < 101:
            print(f'{num} is out of range [1, 100]\n')
        else:
            if num % 15 == 0:
                print('>>> FizzBuzz\n')
            elif num % 5 == 0:
                print('>>> Buzz\n')
            elif num % 3 == 0:
                print('>>> Fizz\n')
            else:
                print(f'>>> {num}\n')


if __name__ == "__main__":
    try:
        fizz_buzz()
    except KeyboardInterrupt:
        print('-1\nProgram has been stopped')
