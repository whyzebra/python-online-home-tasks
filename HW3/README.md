# **Python Home Task 3**

### **Exercise 1 (Validate Point):**
- Write a program that determines whether the Point A(x, y) is in the shaded area or not

<div align="center">
    <img src=shape.png>
    <p><i>shaded area</i></p>
</div>

#### Solution:
Any `y` from range `[0; 1]` belongs to shaded area.  
All `|x|` lower or equel than `y`, from mentioned above range, belongs to shaded area.  
As `|x|` always will be greater or equel to `0`, the final condition will be `abs(x) <= y <= 1`  

#### code:
```python
def validate_point(x: float, y: float) -> bool:
    return abs(x) <= y <= 1


if __name__ == "__main__":
    x = float(input('Enter x: '))
    y = float(input('Enter y: '))

    print('Is point in shadow area:', validate_point(x, y))
```
#### output:
    D:\python-online-home-tasks\HW3>python validate_point.py
    Enter x: -0.35
    Enter y: 0.47
    Is point in shadow area: True

----

### **Exercise 2 (FizzBuzz):**
- Write a program that prints the input number from 1 to 100. But for multiple of three prints "Fizz" instead of the number and for the multiples of five prints "Buzz". For numbers which are multiples for both three and five print "FizzBuzz" 

#### Solution:
The code is self explanatory. Solution based on `if`, `elif` and `else` statements. Also I validate input data to be an integer.
#### code:
```python
def fizz_buzz():
    while True:
        try:
            num = int(input('Enter number: '))
        except ValueError:
            print('Please, enter only int numbers in range [1, 100]\n')
            continue

        if not 0 < num < 101:
            print(f'{num} is out of range [1, 100]\n')
        else:
            if num % 15 == 0:
                print('>>> FizzBuzz\n')
            elif num % 5 == 0:
                print('>>> Buzz\n')
            elif num % 3 == 0:
                print('>>> Fizz\n')
            else:
                print(f'>>> {num}\n')


if __name__ == "__main__":
    try:
        fizz_buzz()
    except KeyboardInterrupt:
        print('-1\nProgram has been stopped')
```
#### output:
    D:\python-online-home-tasks\HW3>python fizz_buzz.py
    Enter number: lol
    Please, enter only int numbers in range [1, 100]

    Enter number: 2
    >>> 2

    Enter number: 3
    >>> Fizz

    Enter number: 5
    >>> Buzz

    Enter number: 45
    >>> FizzBuzz

    Enter number: 150
    150 is out of range [1, 100]

    Enter number: -1
    Program has been stopped

----

### **Exercise 3 (Baccarat):**
- Simulate the round of play for baccarat game (https://en.wikipedia.org/wiki/Baccarat_(card_game))

#### Solution:
I have implemented `Baccarat` class with `@classmethod play()`, which "simulates" one round of game.  
Actually it counts points of cards, provided by user, in infinite loop as many times as needed, till the player exit from game by pressing `crl+c`.  
For data validation and counting player cards score, I implemented "private" class methods `__validate_cards()` and `__get_score()`.  
As data structure for storing cards and points I ued `dict` as the most sutible.
#### code:
```python
class Baccarat(object):
    """
    Baccarat game simulation, which
    counts score according to input cards
    """
    deck = {
        'A': 1,
        'K': 0,
        'Q': 0,
        'J': 0,
        '10': 0,
        '9': 9,
        '8': 8,
        '7': 7,
        '6': 6,
        '5': 5,
        '4': 4,
        '3': 3,
        '2': 2
    }

    @classmethod
    def __validate_cards(cls, player_cards: list) -> bool:
        """Returns 'True' if :player_cards: are in :deck:"""
        return all(card in cls.deck for card in player_cards)

    @classmethod
    def __get_score(cls, player_cards: list) -> int:
        """Returns score of :player_cards:"""
        score = sum(cls.deck[card] for card in player_cards)

        return score if score < 10 else score - 10

    @classmethod
    def play(cls):
        """Baccarat game loop"""
        print('Baccarat game has been started ', end='')
        print('(Press `Ctrl+C` to stop the game)\n')

        while True:
            player_cards = [
                input('Play first card: ').upper(),
                input('Play second card: ').upper()
            ]

            if cls.__validate_cards(player_cards):
                result = cls.__get_score(player_cards)
            else:
                result = 'Do not cheat!'

            print(f'Your result: {result}\n')


if __name__ == "__main__":
    try:
        Baccarat.play()
    except KeyboardInterrupt:
        print('-1\n\nGame has been finished!')

```
#### output:
    D:\python-online-home-tasks\HW3>python baccarat.py
    Baccarat game has been started (Press `Ctrl+C` to stop the game)

    Play first card: 10
    Play second card: 2
    Your result: 2

    Play first card: A
    Play second card: 8
    Your result: 9

    Play first card: joker
    Play second card: poker
    Your result: Do not cheat!

    Play first card: -1

    Game has been finished!