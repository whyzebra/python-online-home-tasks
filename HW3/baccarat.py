class Baccarat(object):
    """
    Baccarat game simulation, which
    counts score according to input cards
    """
    deck = {
        'A': 1,
        'K': 0,
        'Q': 0,
        'J': 0,
        '10': 0,
        '9': 9,
        '8': 8,
        '7': 7,
        '6': 6,
        '5': 5,
        '4': 4,
        '3': 3,
        '2': 2
    }

    @classmethod
    def __validate_cards(cls, player_cards: list) -> bool:
        """Returns 'True' if :player_cards: are in :deck:"""
        return all(card in cls.deck for card in player_cards)

    @classmethod
    def __get_score(cls, player_cards: list) -> int:
        """Returns score of :player_cards:"""
        score = sum(cls.deck[card] for card in player_cards)

        return score if score < 10 else score - 10

    @classmethod
    def play(cls):
        """Baccarat game loop"""
        print('Baccarat game has been started ', end='')
        print('(Press `Ctrl+C` to stop the game)\n')

        while True:
            player_cards = [
                input('Play first card: ').upper(),
                input('Play second card: ').upper()
            ]

            if cls.__validate_cards(player_cards):
                result = cls.__get_score(player_cards)
            else:
                result = 'Do not cheat!'

            print(f'Your result: {result}\n')


if __name__ == "__main__":
    try:
        Baccarat.play()
    except KeyboardInterrupt:
        print('-1\n\nGame has been finished!')
