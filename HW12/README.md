# **Python Home Task 12**

## **Task 1**
**1a.** Invoke `pwd` to see your current working directory (there should be your home directory).
#### **Output:**
```bash
    whyzebra@ubuntu:~$ pwd
    /home/whyzebra
```

<hr>

**1b.** 
Collect output of these commands:  
* `ls -l /` -> list `root` directory contents in a `long listing format`:
```bash
    whyzebra@ubuntu:~$ ls -l /
    total 76total 76
    lrwxrwxrwx   1 root root     7 сер  1 14:00 bin -> usr/bin
    drwxr-xr-x   4 root root  4096 лис 10 23:54 boot
    drwxr-xr-x   2 root root  4096 сер  1 14:06 cdrom
    drwxr-xr-x  21 root root  4560 лис 11 00:26 dev
    drwxr-xr-x 135 root root 12288 лис 10 23:52 etc
    drwxr-xr-x   4 root root  4096 сер  1 14:06 home
    lrwxrwxrwx   1 root root     7 сер  1 14:00 lib -> usr/lib
    lrwxrwxrwx   1 root root     9 сер  1 14:00 lib32 -> usr/lib32
    lrwxrwxrwx   1 root root     9 сер  1 14:00 lib64 -> usr/lib64
    lrwxrwxrwx   1 root root    10 сер  1 14:00 libx32 -> usr/libx32
    drwx------   2 root root 16384 сер  1 13:59 lost+found
    drwxr-xr-x   3 root root  4096 сер  1 14:44 media
    drwxr-xr-x   2 root root  4096 кві 23  2020 mnt
    drwxr-xr-x   3 root root  4096 сер  1 15:37 opt
    dr-xr-xr-x 270 root root     0 лис 11 00:25 proc
    drwx------   4 root root  4096 сер  1 15:14 root
    drwxr-xr-x  35 root root   940 лис 11 00:21 run
    lrwxrwxrwx   1 root root     8 сер  1 14:00 sbin -> usr/sbin
    drwxr-xr-x   9 root root  4096 жов 11 15:04 snap
    drwxr-xr-x   2 root root  4096 кві 23  2020 srv
    dr-xr-xr-x  13 root root     0 лис 11 00:25 sys
    drwxrwxrwt  21 root root  4096 лис 11 00:42 tmp
    drwxr-xr-x  14 root root  4096 кві 23  2020 usr
    drwxr-xr-x  14 root root  4096 кві 23  2020 var
```

<hr>

* `ls` -> lists information about the FILEs (the current directory by default -> `/home`):
```bash
whyzebra@ubuntu:~$ ls
Desktop  development  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos
```
* `ls ~` -> list user's `home` directory:
```bash
whyzebra@ubuntu:~$ ls ~
Desktop  development  Documents  Downloads  Music  Pictures  Public  snap  Templates  Videos
```

<hr>

* `ls -l`
```bash
whyzebra@ubuntu:~$ ls -l
total 40
drwxr-xr-x  6 whyzebra whyzebra 4096 жов 27 08:08 Desktop
drwxrwxr-x 10 whyzebra whyzebra 4096 жов 25 22:57 development
drwxr-xr-x  2 whyzebra whyzebra 4096 сер  1 14:35 Documents
drwxr-xr-x  4 whyzebra whyzebra 4096 лис 11 00:25 Downloads
drwxr-xr-x  2 whyzebra whyzebra 4096 сер  1 14:35 Music
drwxr-xr-x  2 whyzebra whyzebra 4096 лис  4 00:48 Pictures
drwxr-xr-x  2 whyzebra whyzebra 4096 сер  1 14:35 Public
drwxr-xr-x  3 whyzebra whyzebra 4096 сер  1 14:46 snap
drwxr-xr-x  2 whyzebra whyzebra 4096 сер  1 14:35 Templates
drwxr-xr-x  2 whyzebra whyzebra 4096 сер  1 14:35 Videos
```

<hr>

* `ls -a`
```bash
whyzebra@ubuntu:~$ ls -a
.              .bashrc      .devilspie  .gnupg    .nvidia-settings-rc  Public           .sudo_as_admin_successful
..             .cache       Documents   .local    .pam_environment     .pylint.d        Templates
.atom          .config      Downloads   .mozilla  Pictures             .python_history  Videos
.bash_history  Desktop      .gitconfig  Music     .pki                 snap             .vscode
.bash_logout   development  .gnome      .nv       .profile             .ssh             .xinputrc
```

<hr>

* `ls -la`
```bash
whyzebra@ubuntu:~$ ls -la
total 180
drwxr-xr-x 25 whyzebra whyzebra  4096 лис 11 00:21 .
drwxr-xr-x  4 root     root      4096 сер  1 14:06 ..
drwxrwxr-x  9 whyzebra whyzebra  4096 сер  4 23:13 .atom
-rw-------  1 whyzebra whyzebra 46457 лис 11 00:21 .bash_history
-rw-r--r--  1 whyzebra whyzebra   220 сер  1 14:06 .bash_logout
-rw-r--r--  1 whyzebra whyzebra  3771 сер  1 14:06 .bashrc
drwxr-xr-x 26 whyzebra whyzebra  4096 сер 12 20:32 .cache
drwxr-xr-x 25 whyzebra whyzebra  4096 лис 10 23:38 .config
drwxr-xr-x  6 whyzebra whyzebra  4096 жов 27 08:08 Desktop
drwxrwxr-x 10 whyzebra whyzebra  4096 жов 25 22:57 development
drwxrwxr-x  2 whyzebra whyzebra  4096 сер  4 02:53 .devilspie
drwxr-xr-x  2 whyzebra whyzebra  4096 сер  1 14:35 Documents
drwxr-xr-x  4 whyzebra whyzebra  4096 лис 11 00:25 Downloads
-rw-rw-r--  1 whyzebra whyzebra   197 лис  2 02:22 .gitconfig
drwx------  3 whyzebra whyzebra  4096 вер  6 12:07 .gnome
drwx------  3 whyzebra whyzebra  4096 лис  1 21:57 .gnupg
drwxr-xr-x  5 whyzebra whyzebra  4096 сер  1 18:30 .local
drwx------  5 whyzebra whyzebra  4096 сер  1 14:46 .mozilla
drwxr-xr-x  2 whyzebra whyzebra  4096 сер  1 14:35 Music
drwx------  4 whyzebra whyzebra  4096 сер  7 01:34 .nv
-rw-rw-r--  1 whyzebra whyzebra   481 сер  7 18:41 .nvidia-settings-rc
-rw-r--r--  1 whyzebra whyzebra   306 лис 11 00:20 .pam_environment
drwxr-xr-x  2 whyzebra whyzebra  4096 лис  4 00:48 Pictures
drwx------  3 whyzebra whyzebra  4096 сер  1 15:38 .pki
-rw-r--r--  1 whyzebra whyzebra   807 сер  1 14:06 .profile
drwxr-xr-x  2 whyzebra whyzebra  4096 сер  1 14:35 Public
drwxrwxr-x  2 whyzebra whyzebra  4096 жов 24 01:18 .pylint.d
-rw-------  1 whyzebra whyzebra   500 лис 10 23:57 .python_history
drwxr-xr-x  3 whyzebra whyzebra  4096 сер  1 14:46 snap
drwx------  2 whyzebra whyzebra  4096 жов 25 22:38 .ssh
-rw-r--r--  1 whyzebra whyzebra     0 сер  1 15:00 .sudo_as_admin_successful
drwxr-xr-x  2 whyzebra whyzebra  4096 сер  1 14:35 Templates
drwxr-xr-x  2 whyzebra whyzebra  4096 сер  1 14:35 Videos
drwxrwxr-x  3 whyzebra whyzebra  4096 сер  1 17:43 .vscode
-rw-rw-r--  1 whyzebra whyzebra   131 лис  8 11:01 .xinputrc
```

<hr>

* `ls -lda ~`
```bash
whyzebra@ubuntu:~$ ls -lda ~
drwxr-xr-x 25 whyzebra whyzebra 4096 лис 11 00:21 /home/whyzebra
```

<hr>

```bash
```

**1c.** Execute and describe the following commands (store the output, if any)
* `mkdir test` -> creates new directory called `"test"`
```bash
whyzebra@ubuntu:~$ mkdir test
```
* `cd test` -> current working directory changes to `test/`
```bash
whyzebra@ubuntu:~$ cd test
```
* `pwd` -> prints current working directory
```bash
whyzebra@ubuntu:~/test$ pwd 
/home/whyzebra/test
```
* `touch test.txt` -> creates file `test.txt` inside `test/` directory
```bash
whyzebra@ubuntu:~/test$ touch test.txt
```
* `ls -l test.txt` -> displays long listing of the content of current directory such as `owner`, `group owner`, `link count`, `permission`
```bash
whyzebra@ubuntu:~/test$ ls -l test.txt
-rw-rw-r-- 1 whyzebra whyzebra 0 лис 13 08:22 test.txt
```
* `mkdir test2` -> creates new directory called `"test2"`
```bash
whyzebra@ubuntu:~/test$ mkdir test2
```
* `mv test.txt test2` -> moves file `text.txt` from `test/` folder to `test2/`
```bash
whyzebra@ubuntu:~/test$ mv test.txt test2
```
* `cd test2` current working directory changes to `test/`
```bash
whyzebra@ubuntu:~/test$ cd test2/
```
* `ls` -> displays a list of files and or directories
```bash
whyzebra@ubuntu:~/test/test2$ ls
test.txt
```
* `mv test.txt test2.txt` -> overwrites content of file `test2.txt` with `test.txt` and deletes `test.txt`
```bash
whyzebra@ubuntu:~/test/test2$ mv test.txt test2.txt
whyzebra@ubuntu:~/test/test2$ ls
test2.txt
```
* `cp test2.txt ..` -> copy file `test2.txt` to `test` folder
```bash
whyzebra@ubuntu:~/test/test2$ cp test2.txt ..
whyzebra@ubuntu:~/test/test2$ cd ..
whyzebra@ubuntu:~/test$ ls
test2  test2.txt
```
* `rm test2.txt` -> deletes file `test2.txt` from `test` folder
```bash
whyzebra@ubuntu:~/test$ rm test2.txt
```
* `rmdir test2` -> makes an attempt to delete folder `test2/` but fails
in order to fix tihs, we can add `-r` argument: `rm -r test2`
```bash
whyzebra@ubuntu:~/test$ rmdir test2
rmdir: failed to remove 'test2': Directory not empty
```

### Full listing of above commands:
```bash
whyzebra@ubuntu:~$ mkdir test
whyzebra@ubuntu:~$ cd test
whyzebra@ubuntu:~/test$ pwd
/home/whyzebra/test
whyzebra@ubuntu:~/test$ touch test.txt
whyzebra@ubuntu:~/test$ ls -l test.txt
-rw-rw-r-- 1 whyzebra whyzebra 0 лис 13 08:22 test.txt
whyzebra@ubuntu:~/test$ mkdir test2
whyzebra@ubuntu:~/test$ mv test.txt test2
whyzebra@ubuntu:~/test$ cd test2/
whyzebra@ubuntu:~/test/test2$ ls
test.txt
whyzebra@ubuntu:~/test/test2$ mv test.txt test2.txt
whyzebra@ubuntu:~/test/test2$ ls
test2.txt
whyzebra@ubuntu:~/test/test2$ cp test2.txt ..
whyzebra@ubuntu:~/test/test2$ cd ..
whyzebra@ubuntu:~/test$ ls
test2  test2.txt
whyzebra@ubuntu:~/test$ rm test2.txt
whyzebra@ubuntu:~/test$ rmdir test2
rmdir: failed to remove 'test2': Directory not empty
whyzebra@ubuntu:~/test$ 
```

<hr>

**1d.** Execute and describe the difference:
* `cat /etc/fstab` -> displays content of file `/etc/fstab`
```bash
whyzebra@ubuntu:~/test$ cat /etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda7 during installation
UUID=f54e79bb-f74b-411c-934c-67d23c2907d8 /               ext4    errors=remount-ro 0       1
# /boot/efi was on /dev/sda1 during installation
UUID=FE50-9A64  /boot/efi       vfat    umask=0077      0       1
# /home was on /dev/sda8 during installation
UUID=a4c1a673-b83f-4771-baeb-9177142a77bd /home           ext4    defaults        0       2
# swap was on /dev/sda6 during installation
UUID=e97385a6-55f5-451d-9b13-41a80a86263a none            swap    sw              0       0
```

<hr>

* `less /etc/fstab` -> displays the contents of a file or a command output, one page at a time,  allows to navigate `forward` and `backward` through the file.  
Also, `less` does not have to read the entire input file before starting, so with large input files it starts up faster than text editors.  
`less` is faster than `more` because it does not load the entire file. On example file is too small so it fits into one "page".

<div align="center">
    <img src="less cmd example.png">
    <pre>^ less /etc/fstab ^</pre>
</div>


<hr>

* `more /etc/fstab` -> displays the contents of a file on one screen at a time in case the file is large.
```bash
whyzebra@ubuntu:~/test$ more /etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda7 during installation
UUID=f54e79bb-f74b-411c-934c-67d23c2907d8 /               ext4    errors=remount-ro 0       1
# /boot/efi was on /dev/sda1 during installation
UUID=FE50-9A64  /boot/efi       vfat    umask=0077      0       1
# /home was on /dev/sda8 during installation
UUID=a4c1a673-b83f-4771-baeb-9177142a77bd /home           ext4    defaults        0       2
# swap was on /dev/sda6 during installation
UUID=e97385a6-55f5-451d-9b13-41a80a86263a none            swap    sw              0       0
```

<div align="center">
    <img src="more cmd example.png">
    <pre>press "Enter" several times to scroll down</pre>
    <img src="more cmd example 2.png">
</div>


## **Task 2**

*2a.* Discovering soft and hard links. 
<div align="center">
    <img src="task 2.png">
</div>

<hr>

**2b.** Dealing with chmod.  
An executable script. Open your favorite editor and put these lines into a file 
```bash
# !/bin/bash
echo "Drugs are bad MKAY?"
```
Give name “script.sh” to the script and call to
* `chmod +x script.sh`
Then you are ready to execute the script:
* `./script.sh`

```bash
whyzebra@ubuntu:~/development/python-online-home-tasks/HW12$ . script.sh
Drugs are bad MKAY?
```
<div align="center">
    <img src="script output.png">
</div>


