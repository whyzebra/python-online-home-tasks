def combine_dicts(*args: dict) -> dict:
    """
    Return 'combined' dictionary with
    summarized values of identical keys
    """
    combined = args[0]

    for dictionary in args[1:]:
        for key, value in dictionary.items():
            combined[key] = combined.get(key, 0) + value

    return combined


if __name__ == "__main__":
    args = [
        {'a': 100, 'b': 300},
        {'a': 200, 'b': 600},
        {'a': 300, 'b': 900, 'c': 200},
        {'b': 900, 'c': 800}
    ]
    print('dicts to combine:')
    print(' + '.join(list(map(str, args))))
    print(f'\nresult: {combine_dicts(*args)}')
