# **Python Home Task 6**

### **Exercise 1 (combine_dicts):**
Implement a function, that receives changeable number of dictionaries (keys - letters, values - numbers) and combines them into one dictionary.  
Dict values should be summarized in case of identical keys.

### Solution:
For this exercise I used the simplest and the fastest possible solution:  
I iterate over each `dict` inside `args`, add these keys to `combined_dict`  
and summarize all values with identical keys inside `combined_dict`

### code:
```python
def combine_dicts(*args: dict) -> dict:
    """
    Return 'combined' dictionary with
    summarized values of identical keys
    """
    combined = args[0]

    for dictionary in args[1:]:
        for key, value in dictionary.items():
            combined[key] = combined.get(key, 0) + value

    return combined


if __name__ == "__main__":
    args = [
        {'a': 100, 'b': 300},
        {'a': 200, 'b': 600},
        {'a': 300, 'b': 900, 'c': 200},
        {'b': 900, 'c': 800}
    ]
    print('dicts to combine:')
    print(' + '.join(list(map(str, args))))
    print(f'\nresult: {combine_dicts(*args)}')
```
#### output:
    D:\python-online-home-tasks\HW6>python combine_dicts.py
    dicts to combine:
    {'a': 100, 'b': 300} + {'a': 200, 'b': 600} + {'a': 300, 'b': 900, 'c': 200} + {'b': 900, 'c': 800}

    result: {'a': 600, 'b': 2700, 'c': 1000}
----

### **Exercise 2 (CustomList):**
To create generic type CustomList - the list of values, which has length that is extended when new elements are added to the list.

CustomList is a collection — the list of values of random type, its size changes dynamically and there is a possibility to index list elements. Indexation in the list starts with 0.

Values of random type can be located in the list, it should be created empty and a set of original values should be specified. List length changes while
adding and removing elements. The elements can be added and removed using specific methods. List can be checked whether there is a predetermined value in the list.

List indexing allows to perform the following operations based on indexes:

- To change and read values of existing elements by using indexer
- To receive index of predetermined value
- To remove value based on index

The list can be cleared, its length can be identified, its elements can be received in the form of linked list Item that is available via link to head.
Collection CustomList can be used in operator foreach and other constructions that are oriented to the presence of numerator in class.

The task has two levels of complexity: Low and Advanced.

**Low level tasks** require implementation of the following functionality:
* Creating of empty user list and the one based on elements set (the elements are stored in CustomList in form of unidirectional linked list
* Adding, removing elements
* Operations with elements by index
* Clearing the list, receiving its length
* Receiving link to linked elements list

**Advanced level tasks** require implementation of the following functionality:
* All completed tasks of Low level
* Generating exceptions, specified in xml-comments to class methods
* Receiving from numerator list for operator foreach

### Solution:
My ***`CustomList`*** is a representation of classic ***`Simple Linked List`***.  
It provides the link to `HEAD Node`, `TAIL Node` and stores `length` (amount of all Nodes). 
<br>
<br>
<h2><center>For My <b>CustomList</b> I have implemented next methods:</center></h2>

- `prepend(data: Any)` -> Add new HEAD Node, takes any object as `data`;
- `append(new_data: Any)` -> Add Node to the END of thelist, takes any object as `data`;
- `extend(*data)` -> Append all items inside tuple `data`;
- `insert(node_index: int, data: Any)` -> Insert a `data` as new `Node` at a given position `node_index`;
- `remove(data: Any)` -> Remove the first node with value equal to `data`;

- `pop(node_index: int)` -> Remove a `Node` with a given position and return its `value`,  
depending on the Node position, there are 3 methods to optimize this operation:
    1. `__pop_head()` -> Remove `HEAD Node` and return its `value`;
    1. `__pop_inner(node_index: int)` -> Remove `Node` with `node_index` and return its `value`;
    1. `__pop_tail()` -> Remove `TAIL Node` and return its `value`;

- `clear()` -> Remove all items from the `CustomList`;
- `index(data: Any)` -> Return index of first `Node` equals to `:data`;
- `count(data: Any)` -> Return the number of times `data` appears in the `CustomList`;
- `reverse()` -> Reverse the `Nodes order` in the `CustomList`.

<br>
<h2><center>Also I`ve overrided next class <b>magic methods</b>:</center></h2>

- `__len__()` -> Return list `length` as `int`;
- `__repr__()` -> Represents `CustomList` as string of all `Node.data`, separated with `>>>` chars;
- `__iter__()` -> `yield` all `Nodes` till `TAIL Node` is reached;
- `__setitem__(node_index: int, new_data: Any)` -> Set any `data` to `Node`, supports `positive` and `negative` **indexing**;
- `__getitem__(node_index: int)` -> Return Node value, supports `positive` and `negative` **indexing**.


### code:
```python
from __future__ import annotations
from typing import Any


class Node:
    def __init__(self, data: Any):
        self.data = data
        self.next = None

    def __repr__(self):
        return f'{self.data}'


class CustomList:
    def __init__(self, *init_data: Any):
        self.head = None
        self.tail = self.head
        self.length = 0

        if init_data:
            self.head = Node(init_data[0])
            node = self.head
            self.__inc_len()

            for data in init_data[1:]:
                node.next = Node(data)
                node = node.next
                self.__inc_len()

            self.tail = node

    def __len__(self):
        return self.length

    def __repr__(self):
        nodes_data = [f'{node.data}' for node in self]
        nodes_data.append('None')

        return ' >>> '.join(nodes_data)

    def __iter__(self):
        node = self.head

        while node:
            yield node
            node = node.next

    def __setitem__(self, node_index: int, new_data: Any):
        self.__validate_index(node_index)
        self.__get_node_by_index(node_index).data = new_data

    def __getitem__(self, node_index: int):
        if isinstance(node_index, slice):
            start, stop, step = node_index.indices(self.length)
            return CustomList(*[self[i] for i in range(start, stop, step)])

        self.__validate_index(node_index)
        return self.__get_node_by_index(node_index)

    def __get_node_by_index(self, node_index: int) -> Node:
        """Return node by :node_index:"""
        if node_index == 0:
            return self.head

        if node_index == self.length - 1:
            return self.tail

        if node_index < 0:
            node_index = self.length - abs(node_index)

        for index, node in enumerate(self):
            if node_index == index:
                return node

    def __inc_len(self):
        """Increments length of CustomList by one"""
        self.length += 1

    def __dec_len(self):
        """Decrements length of CustomList by one"""
        self.length -= 1

    def __validate_for_empty_instacne(self, new_data):
        if self.length > 0:
            return True

        self.head = Node(new_data)
        self.tail = self.head
        self.__inc_len()
        return False

    def prepend(self, data: Any):
        """Add new head node"""
        if self.__validate_for_empty_instacne(data):
            new_head = Node(data)
            new_head.next = self.head
            self.head = new_head
            self.__inc_len()

    def append(self, new_data: Any):
        """Add node to the end of the CustomList"""
        if self.__validate_for_empty_instacne(new_data):
            self.tail.next = Node(new_data)
            self.tail = self.tail.next
            self.__inc_len()

    def extend(self, *data):
        """Append all items given as parameters"""
        for item in data:
            self.append(item)

    def insert(self, node_index: int, data: Any):
        """Insert a node at a given position"""
        if not isinstance(node_index, int):
            raise ValueError('Linked List indices must be integers or slices, not str')

        if node_index <= 0:
            raise IndexError(f'CustomList index {node_index} is out of range')

        if node_index == 0:
            self.prepend(data)

        elif node_index >= self.length:
            self.append(data)

        else:
            prev_node = self.__get_node_by_index(node_index-1)
            new_node = Node(data)
            new_node.next = prev_node.next
            prev_node.next = new_node

            self.__inc_len()

    def remove(self, data: Any):
        """Remove the first node with value equal to :data:"""
        self.pop(self.index(data))

    def pop(self, node_index: int) -> Node.data:
        """Remove a node with a given position and return its value"""
        self.__validate_index(node_index)

        pop_data = None

        if node_index == 0:
            pop_data = self.__pop_head()

        elif node_index == self.length - 1:
            pop_data = self.__pop_tail()

        else:
            pop_data = self.__pop_inner(node_index)

        self.__dec_len()
        return pop_data

    def __pop_head(self) -> Node.data:
        """Remove head node and return its value"""
        pop_data = self.head.data
        self.head = self.head.next
        return pop_data

    def __pop_inner(self, node_index: int) -> Node.data:
        """Remove node with :node_index: and return its value"""
        prev_node = self.__get_node_by_index(node_index-1)
        pop_data = prev_node.next.data
        prev_node.next = prev_node.next.next
        return pop_data

    def __pop_tail(self) -> Node.data:
        """Remove tail node and return its value"""
        pop_data = self.tail.data

        self.tail = self.__get_node_by_index(self.length-2)
        self.tail.next = None

        return pop_data

    def clear(self):
        """Remove all items from the CustomList"""
        self.head = None
        self.tail = self.head
        self.length = 0

    def index(self, data: Any) -> int:
        """Return index of first node equals to :data:"""
        node_index = 0

        for node in self:
            if node.data == data:
                return node_index

            node_index += 1

        raise ValueError(f'{data} is not in Linked List')

    def count(self, data: Any) -> int:
        """Return the number of timex :data: appears in the CustomList"""
        counter = 0

        for node in self:
            if node.data == data:
                counter += 1

        return counter

    def reverse(self):
        """Reverse the nodes order in the CustomList"""
        prev_node = None
        current_node = self.head
        self.tail = current_node

        while current_node is not None:
            next_node = current_node.next
            current_node.next = prev_node
            prev_node = current_node
            current_node = next_node

        self.head = prev_node

    def __validate_index(self, node_index: int):
        """Raises IndexError if :node_index: is out of range"""
        if not isinstance(node_index, int):
            raise TypeError('Linked List indices must be integers or slices, not str')

        if abs(node_index) > self.length:
            raise IndexError(f'CustomList index {node_index} is out of range')


if __name__ == '__main__':
    print('car = CustomList()')
    car = CustomList()
    print(f'car: {car}\n')

    print("car.append('Supra')")
    car.append('Supra')
    print(f'car: {car}\n')

    print("car.insert(1, 'Nissan')")
    car.insert(1, 'Nissan')
    print(f'car: {car}\n')

    print("car[car.index('Nissan')] = 'Toyota'")
    car[car.index('Nissan')] = 'Toyota'
    print(f'car: {car}\n')

    print("car.prepend('2JZ')")
    car.prepend('2JZ')
    print(f'car: {car}\n')

    print("car.reverse()")
    car.reverse()
    print(f'car: {car}\n')

    print("car.extend(['GTE', 'VVTi'], {'year': 2002, 'price': '50000$'}, 'Turbo', 'Turbo')")
    car.extend(['GTE', 'VVTi'], {'year': 2002, 'price': '50000$'}, 'Turbo', 'Turbo')
    print(f'car: {car}\n')

    print(f"car.pop(4) -> {car.pop(4)}")
    print(f'car: {car}\n')

    print("car.count('Turbo')")
    print(f'car has {car.count("Turbo")} turbos\n')

    print(f'car: {car}')
    print(f'len(car) -> {len(car)}\n')

    print(f'car[0] -> {car[0]}')
    print(f'car[-3] -> {car[-3]}')
    print(f'car[1:4] -> {car[1:4]}')
    print(f'car[::-1] -> {car[::-1]}\n')

    print("car.clear()")
    car.clear()
    print(f'car: {car}')
```
#### output:
    D:\python-online-home-tasks\HW6>python custom_list.py
    car = CustomList()
    car: None

    car.append('Supra')
    car: Supra >>> None

    car.insert(1, 'Nissan')
    car: Supra >>> Nissan >>> None

    car[car.index('Nissan')] = 'Toyota'
    car: Supra >>> Toyota >>> None

    car.prepend('2JZ')
    car: 2JZ >>> Supra >>> Toyota >>> None

    car.reverse()
    car: Toyota >>> Supra >>> 2JZ >>> None

    car.extend(['GTE', 'VVTi'], {'year': 2002, 'price': '50000$'}, 'Turbo', 'Turbo')
    car: Toyota >>> Supra >>> 2JZ >>> ['GTE', 'VVTi'] >>> {'year': 2002, 'price': '50000$'} >>> Turbo >>> Turbo >>> None

    car.pop(4) -> {'year': 2002, 'price': '50000$'}
    car: Toyota >>> Supra >>> 2JZ >>> ['GTE', 'VVTi'] >>> Turbo >>> Turbo >>> None

    car.count('Turbo')
    car has 2 turbos

    car: Toyota >>> Supra >>> 2JZ >>> ['GTE', 'VVTi'] >>> Turbo >>> Turbo >>> None
    len(car) -> 6

    car[0] -> Toyota
    car[-3] -> ['GTE', 'VVTi']
    car[1:4] -> Supra >>> 2JZ >>> ['GTE', 'VVTi'] >>> None
    car[::-1] -> Turbo >>> Turbo >>> ['GTE', 'VVTi'] >>> 2JZ >>> Supra >>> Toyota >>> None

    car.clear()
    car: None
