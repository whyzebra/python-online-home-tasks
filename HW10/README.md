# **Python Home Task 10**

### **Task 1**
Open file **`data/unsorted_names.txt`** in data folder.  
Sort the names and write them to a new file called **`sorted_names.txt`**.  
Each name should start with a new line as in the following example:
```
Adele
Adrienne
...
Willodean
Xavier
```
### Solution:
Solution is self explanatory:
1. I read all lines from file using `with` statement and save them to variable `data`;  
1. I open/create new file using `with` statement with `w+` mode  and write sorted data using python built-in functionality `sorted(data)`.  

Program output is located at ["**data/sorted_names.txt**"](https://gitlab.com/whyzebra/python-online-home-tasks/-/blob/master/HW10/data/sorted_names.txt).
### Code:
```python
UNSORTED_NAMES_PATH = 'data/unsorted_names.txt'
SORTED_NAMES_PATH = 'data/sorted_names.txt'


def sort_names(read_from=UNSORTED_NAMES_PATH, save_to=SORTED_NAMES_PATH) -> bool:
    """
    Sort the names from file path :read_from: and write them to :save_to:
    """
    with open(read_from, 'r') as file:
        data = file.readlines()

    with open(save_to, 'w+') as new_file:
        new_file.writelines(sorted(data))

    return True


if __name__ == "__main__":
    if sort_names():
        print(f'sorted names saved to {SORTED_NAMES_PATH}')
```
### output:
    D:\python-online-home-tasks\HW10>python sort_names.py
    sorted names saved to data/sorted_names.txt
----

### **Task 2**
Implement a function which search for **most common words** in the file.  
Use **`data/lorem_ipsum.txt`** file as an example.
```python
def most_common_words(filepath, number_of_words=3):
    pass
print(most_common_words('lorem_ipsum.txt'))
>>> ['donec', 'etiam', 'aliquam']
```
_**NOTE:** Remember about dots, commas, capital letters etc_
### Solution:
For this task I've implemented class `MostCommonWords`, with two `@static` methods for validating input and two `@classmethods`  
for searching most commong words.
_Static_ methods are self explanatory, so I will describe only _classmethods_

### 1. `@classmethod pythonic_way`:  
This method is based only on buil-in functionality and has zero explicit
 `for loops`, there are 3 main steps:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.1 Read all lines from file, opened by `with`, and transform them to lowercase;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.2 Remove all punctuation signs and left just plain lowercase text with help of `translate` method together with `str.maketarns`;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.3 Split plain text by `whitespace` and saved to list, called `wordlist`;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.4 Count most common words in the `wordlist`, by means of built-in method `Counter.most_common()` from module `collections`.

### 2. `@classmethod custom_approach`:  
This is implementation of the same task, but without module collections (only `defaultdict` is used).  
Method works by next steps:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.1 Read all lines from file, opened by `with`, and transform them to lowercase;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.2 Iterate over each char in text, re-construct raw words without any punctuation marks and  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;save occurances num of these raw words to `defaultdict`, called `counter` _(**key:** word, **value:** word occurances in text)_;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.3 Generate list `top_occurances` with keys of `counter`, sorted in descending order;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.4 Iterate over each word in `counter` and add it to `most_common` list if word occurances number is in `top_occurances`  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;in this case - remove number from `top_occurances`
### Code:
```python
from collections import Counter, defaultdict
import string
import os

DEFAULT_PATH = 'data/lorem_ipsum.txt'

class MostCommonWords:
    @staticmethod
    def __validate_filepath(filepath):
        """
        :raises FileNotFoundError: if :param filepath: does not exist
        :return: True in case of success
        """
        if not os.path.exists(filepath):
            raise FileNotFoundError(f"'{filepath}' does not exist")

        return True

    @staticmethod
    def __validate_number_of_words(number_of_words):
        """
        :raises TypeError: if :param number_of_words: is not 'int'
        :raises ValueError: for :param number_of_words: <= 0
        :return: True in case of success
        """
        if not isinstance(number_of_words, int):
            raise TypeError(f":number_of_words: must be type 'int', not '{type(number_of_words).__name__}'")

        if number_of_words <= 0:
            raise ValueError(":number_of_words: must be > 0")

        return True

    @classmethod
    def pythonic_way(cls, filepath, number_of_words=3):
        """
        Return list of most common words in the file
        Based only on built-in packages without for loops

        :param filepath: path to file with text
        :param number_of_words: nubmber of words to show
        """
        cls.__validate_filepath(filepath)
        cls.__validate_number_of_words(number_of_words)

        with open(filepath, 'r') as file:
            text = file.read().lower()

        trans_table = str.maketrans('', '', string.punctuation + '\n')
        plain_text = text.translate(trans_table)
        wordlist = plain_text.split()

        most_common = Counter(wordlist).most_common(number_of_words)

        return [item[0] for item in most_common]


    @classmethod
    def custom_approach(cls, filepath, number_of_words=3):
        """
        Return list of most common words in the file
        None of built-in packages were used for counting words

        :param filepath: path to file with text
        :param number_of_words: nubmber of words to show
        """
        cls.__validate_filepath(filepath)
        cls.__validate_number_of_words(number_of_words)

        with open(filepath, 'r') as file:
            text = file.read().lower()

        word = ''
        wordlist = defaultdict(int)

        for char in text:
            if char.isalnum():
                word += char
            elif word != '':
                wordlist[word] += 1
                word = ''

        top_occurances = sorted(dict(wordlist).values(), reverse=True)[:number_of_words]
        most_common_words = []

        for word in wordlist:
            if wordlist[word] in top_occurances:
                most_common_words.append(word)
                top_occurances.remove(wordlist[word])

        return most_common_words


if __name__ == "__main__":
    number_of_words = 3

    print(f"Most common {number_of_words} words in '{DEFAULT_PATH}':")
    print('pythonic_way:\t ', end='')
    print(MostCommonWords.pythonic_way(DEFAULT_PATH, number_of_words))

    print('custom_approach: ', end='')
    print(MostCommonWords.custom_approach(DEFAULT_PATH, number_of_words))
```
### output:
    D:\python-online-home-tasks\HW10>python most_common_words.py
    Most common 3 words in 'data/lorem_ipsum.txt':
    pythonic_way:    ['donec', 'etiam', 'aliquam']
    custom_approach: ['donec', 'etiam', 'aliquam']
----

### **Task 3** 
File **`data/students.csv`** stores information about students in **[CSV](https://en.wikipedia.org/wiki/Comma-separated_values)** format.  
This file contains the `student’s names`, `age` and `average mark`. 
1) Implement a function which receives file path and returns names of top performer students
    ```python
    def get_top_performers(file_path, number_of_top_students=5):
        pass
    print(get_top_performers("students.csv"))
    >>> ['Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia', 'Joseph Head']
    ```
2) Implement a function which receives the file path with students info and writes CSV student information to the new file in descending order of age. Result:
    ```student name, age, average mark
    Verdell Crawford, 30, 8.86
    Brenda Silva, 30, 7.53
    ...
    Lindsey Cummings, 18, 6.88
    Raymond Soileau,18,7.27
    ```
### Solution:
For this task I've implemented class `StundentData` with bunch of methods, which helps to `read` /  `write` csv and  
`sort` object `csv_dict_reader` by specified field and save it as `.csv` file

### **1. `get_top_performers()`**
This method returns the list of names of top performer students. Methods perform next actions:  
1. Read `.csv` file with students to `DictReader` object;
1. Sort `csv_dict_reader` object by field `average mark`;
1. Get nams of specified amount of top sudents, by slicing the sorted data and getting `studanrt name`.

### **2. `save_students_sorted_by_age()`**
1. Read `.csv` file with students to `DictReader` object;
1. Sort `csv_dict_reader` object by field `age`;
1. Write `csv_dict_reader` object to new file.
### Code:
```python
import csv
import os


class StudentsData:
    @staticmethod
    def __validate_filepath(filepath):
        """
        :raises FileNotFoundError: if :param filepath: does not exist
        :return: True in case of success
        """
        if not os.path.exists(filepath):
            raise FileNotFoundError(f"'{filepath}' does not exist")

        return True

    @staticmethod
    def __validate_number_of_top_students(number_of_students):
        """
        :raises TypeError: if :param number_of_students: is not 'int'
        :raises ValueError: for :param number_of_students: <= 0
        :return: True in case of success
        """
        if not isinstance(number_of_students, int):
            raise TypeError(f":number_of_students: must be type 'int', not '{type(number_of_students).__name__}'")

        if number_of_students <= 0:
            raise ValueError(":number_of_students: must be > 0")

        return True

    @staticmethod
    def __get_students(filepath):
        with open(filepath, 'r') as csv_file:
            return csv.DictReader(csv_file.readlines(), delimiter=',')

    @staticmethod
    def __sort_csv_dict(csv_dict_reader, sort_by, reverse=True):
        return sorted(csv_dict_reader, key=lambda item: float(item[sort_by]), reverse=reverse)

    @staticmethod
    def __write_csv_from_dict(filepath, csv_dict, fieldnames):
        with open(filepath, 'w+', newline='') as new_csv:
            writer = csv.DictWriter(new_csv, fieldnames=fieldnames)
            writer.writeheader()

            writer.writerows(csv_dict)

    @classmethod
    def get_top_performers(cls, filepath, number_of_top_students=5):
        """
        Return list of names of top performer students

        :param filepath: file to .csv file with students data
        :param number_of_top_students: number of TOP students to show, default = 5
        """
        cls.__validate_filepath(filepath)
        cls.__validate_number_of_top_students(number_of_top_students)

        students_csv_dict = cls.__get_students(filepath)
        student_sorted_by_average_mark = cls.__sort_csv_dict(students_csv_dict, sort_by='average mark')
        top_students = student_sorted_by_average_mark[:number_of_top_students]

        return [student['student name'] for student in top_students]

    @classmethod
    def save_students_sorted_by_age(cls, filepath, save_to):
        """
        Writes new .csv file, with students data sorted by age
        in descndng order (from oldest to youngest student)

        :param filepath: file to .csv file with students data
        :param save_to: path to output file
        """
        cls.__validate_filepath(filepath)
        cls.__validate_filepath(save_to)

        students_csv_dict = cls.__get_students(filepath)
        students_sorted_by_age = cls.__sort_csv_dict(students_csv_dict, sort_by='age')
        cls.__write_csv_from_dict(save_to, students_sorted_by_age, students_csv_dict.fieldnames)


STUDENTS_PATH = 'data/students.csv'
SORTED_BY_AGE_PATH = 'data/students_sorted_by_age.csv'


if __name__ == "__main__":
    number_of_top_students = 5
    top_students = StudentsData.get_top_performers(STUDENTS_PATH, number_of_top_students)
    print(f'{number_of_top_students} TOP students: {top_students}')

    StudentsData.save_students_sorted_by_age(STUDENTS_PATH, SORTED_BY_AGE_PATH)

```
### output:
    D:\python-online-home-tasks\HW10>python get_top_performers.py
    5 TOP students: ['Josephina Medina', 'Teresa Jones', 'Richard Snider', 'Jessica Dubose', 'Heather Garcia']