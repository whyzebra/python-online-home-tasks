UNSORTED_NAMES_PATH = 'data/unsorted_names.txt'
SORTED_NAMES_PATH = 'data/sorted_names.txt'


def sort_names(read_from=UNSORTED_NAMES_PATH, save_to=SORTED_NAMES_PATH) -> bool:
    """
    Sort the names from file path :read_from: and write them to :save_to:
    """
    with open(read_from, 'r') as file:
        data = file.readlines()

    with open(save_to, 'w+') as new_file:
        new_file.writelines(sorted(data))

    return True


if __name__ == "__main__":
    if sort_names():
        print(f'sorted names saved to {SORTED_NAMES_PATH}')
