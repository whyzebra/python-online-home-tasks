import csv
import os


class StudentsData:
    @staticmethod
    def __validate_filepath(filepath):
        """
        :raises FileNotFoundError: if :param filepath: does not exist
        :return: True in case of success
        """
        if not os.path.exists(filepath):
            raise FileNotFoundError(f"'{filepath}' does not exist")

        return True

    @staticmethod
    def __validate_number_of_top_students(number_of_students):
        """
        :raises TypeError: if :param number_of_students: is not 'int'
        :raises ValueError: for :param number_of_students: <= 0
        :return: True in case of success
        """
        if not isinstance(number_of_students, int):
            raise TypeError(f":number_of_students: must be type 'int', not '{type(number_of_students).__name__}'")

        if number_of_students <= 0:
            raise ValueError(":number_of_students: must be > 0")

        return True

    @staticmethod
    def __get_students(filepath):
        with open(filepath, 'r') as csv_file:
            return csv.DictReader(csv_file.readlines(), delimiter=',')

    @staticmethod
    def __sort_csv_dict(csv_dict_reader, sort_by, reverse=True):
        return sorted(csv_dict_reader, key=lambda item: float(item[sort_by]), reverse=reverse)

    @staticmethod
    def __write_csv_from_dict(filepath, csv_dict, fieldnames):
        with open(filepath, 'w+', newline='') as new_csv:
            writer = csv.DictWriter(new_csv, fieldnames=fieldnames)
            writer.writeheader()

            writer.writerows(csv_dict)

    @classmethod
    def get_top_performers(cls, filepath, number_of_top_students=5):
        """
        Return list of names of top performer students

        :param filepath: file to .csv file with students data
        :param number_of_top_students: number of TOP students to show, default = 5
        """
        cls.__validate_filepath(filepath)
        cls.__validate_number_of_top_students(number_of_top_students)

        students_csv_dict = cls.__get_students(filepath)
        student_sorted_by_average_mark = cls.__sort_csv_dict(students_csv_dict, sort_by='average mark')
        top_students = student_sorted_by_average_mark[:number_of_top_students]

        return [student['student name'] for student in top_students]

    @classmethod
    def save_students_sorted_by_age(cls, filepath, save_to):
        """
        Writes new .csv file, with students data sorted by age
        in descndng order (from oldest to youngest student)

        :param filepath: file to .csv file with students data
        :param save_to: path to output file
        """
        cls.__validate_filepath(filepath)
        cls.__validate_filepath(save_to)

        students_csv_dict = cls.__get_students(filepath)
        students_sorted_by_age = cls.__sort_csv_dict(students_csv_dict, sort_by='age')
        cls.__write_csv_from_dict(save_to, students_sorted_by_age, students_csv_dict.fieldnames)


STUDENTS_PATH = 'data/students.csv'
SORTED_BY_AGE_PATH = 'data/students_sorted_by_age.csv'


if __name__ == "__main__":
    number_of_top_students = 5
    top_students = StudentsData.get_top_performers(STUDENTS_PATH, number_of_top_students)
    print(f'{number_of_top_students} TOP students: {top_students}')

    StudentsData.save_students_sorted_by_age(STUDENTS_PATH, SORTED_BY_AGE_PATH)
