from collections import Counter, defaultdict
import string
import os

DEFAULT_PATH = 'data/lorem_ipsum.txt'

class MostCommonWords:
    @staticmethod
    def __validate_filepath(filepath):
        """
        :raises FileNotFoundError: if :param filepath: does not exist
        :return: True in case of success
        """
        if not os.path.exists(filepath):
            raise FileNotFoundError(f"'{filepath}' does not exist")

        return True

    @staticmethod
    def __validate_number_of_words(number_of_words):
        """
        :raises TypeError: if :param number_of_words: is not 'int'
        :raises ValueError: for :param number_of_words: <= 0
        :return: True in case of success
        """
        if not isinstance(number_of_words, int):
            raise TypeError(f":number_of_words: must be type 'int', not '{type(number_of_words).__name__}'")

        if number_of_words <= 0:
            raise ValueError(":number_of_words: must be > 0")

        return True

    @classmethod
    def pythonic_way(cls, filepath, number_of_words=3):
        """
        Return list of most common words in the file
        Based only on built-in packages without for loops

        :param filepath: path to file with text
        :param number_of_words: nubmber of words to show
        """
        cls.__validate_filepath(filepath)
        cls.__validate_number_of_words(number_of_words)

        with open(filepath, 'r') as file:
            text = file.read().lower()

        trans_table = str.maketrans('', '', string.punctuation + '\n')
        plain_text = text.translate(trans_table)
        wordlist = plain_text.split()

        most_common = Counter(wordlist).most_common(number_of_words)

        return [item[0] for item in most_common]


    @classmethod
    def custom_approach(cls, filepath, number_of_words=3):
        """
        Return list of most common words in the file
        None of built-in packages were used for counting words

        :param filepath: path to file with text
        :param number_of_words: nubmber of words to show
        """
        cls.__validate_filepath(filepath)
        cls.__validate_number_of_words(number_of_words)

        with open(filepath, 'r') as file:
            text = file.read().lower()

        word = ''
        wordlist = defaultdict(int)

        for char in text:
            if char.isalnum():
                word += char
            elif word != '':
                wordlist[word] += 1
                word = ''

        top_occurances = sorted(dict(wordlist).values(), reverse=True)[:number_of_words]
        most_common_words = []

        for word in wordlist:
            if wordlist[word] in top_occurances:
                most_common_words.append(word)
                top_occurances.remove(wordlist[word])

        return most_common_words


if __name__ == "__main__":
    number_of_words = 3

    print(f"Most common {number_of_words} words in '{DEFAULT_PATH}':")
    print('pythonic_way:\t ', end='')
    print(MostCommonWords.pythonic_way(DEFAULT_PATH, number_of_words))

    print('custom_approach: ', end='')
    print(MostCommonWords.custom_approach(DEFAULT_PATH, number_of_words))
