import sys
import array_rectangles_driver as driver


class InitValueError(Exception):
    """
    Exception raised during initializing ArrayRectangles with wrong `int` argument

    Attributes:
        max_length -- `int` argument (free_space) which value caused
        the error during `ArrayRectangles` initialization
        message -- explanation of the error
    """

    def __init__(self, max_length, message="ArrayRectangles must be initialized with \'int\' > 0"):
        self.max_length = max_length
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.max_length} -> {self.message}'


class InitTypeError(Exception):
    """
    Exception raised during initializing ArrayRectangles with objects that are not instance of `Rectangle`

    Attributes:
        wrong_type -- type of object that caused the error during `ArrayRectangles` initialization
        message -- explanation of the error
    """

    def __init__(self, wrong_type, message="all elements of an ArrayRectangles must be \'Rectangle\'"):
        self.wrong_type = wrong_type
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.wrong_type} -> {self.message}'


class Rectangle:
    def __init__(self, A, B=5.0):
        self.__side_A = float(A)
        self.__side_B = float(B)

    def __repr__(self):
        return f'Rectangle({self.__side_A}, {self.__side_B})'

    def get_side_A(self):
        return self.__side_A

    def get_side_B(self):
        return self.__side_B

    def area(self):
        return self.__side_A * self.__side_B

    def perimeter(self):
        return (self.__side_A + self.__side_B) * 2

    def is_square(self):
        return self.__side_A == self.__side_B

    def replace_sides(self):
        self.__side_A, self.__side_B = self.__side_B, self.__side_A


class ArrayRectangles:
    def __init__(self, *args):
        self.__array_rectangles = []
        self.__free_space = 0
        self.__next_free_index = None

        self.__max_area = 0
        self.__number_max_area = 0

        self.__min_perimeter = sys.maxsize
        self.__number_min_perimeter = 0

        self.__number_square = 0

        if not args:
            self.__init_empty_array(array_length=5)

        if len(args) == 1:
            if not isinstance(args[0], (int, list, Rectangle)):
                raise InitTypeError(type(args[0]).__name__)

            if isinstance(args[0], int):
                if args[0] <= 0:
                    raise InitValueError(args[0])

                self.__init_empty_array(array_length=args[0])

            elif isinstance(args[0], list):
                self.__init_predefined_array(rectangles=args[0])

            elif isinstance(args[0], Rectangle):
                self.__init_predefined_array(rectangles=[args[0]])

        elif isinstance(args, tuple):
            self.__init_predefined_array(rectangles=args)

    def __init_empty_array(self, array_length):
        self.__free_space = array_length
        self.__next_free_index = 0

    def __init_predefined_array(self, rectangles):
        for rect_index, rect in enumerate(rectangles):
            if not isinstance(rect, Rectangle):
                raise InitTypeError(type(rect).__name__)

            else:
                self.__array_rectangles.append(rect)
                self.__update_array_stats(rect, rect_index)

    def __update_array_stats(self, rect, rect_index):
        if rect.is_square():
            self.__number_square += 1

        if rect.perimeter() < self.__min_perimeter:
            self.__min_perimeter = rect.perimeter()
            self.__number_min_perimeter = rect_index

        if rect.area() > self.__max_area:
            self.__max_area = rect.area()
            self.__number_max_area = rect_index

    def __repr__(self):
        return str(self.__array_rectangles)

    def __iter__(self):
        for rect in self.__array_rectangles:
            yield rect

    def __getitem__(self, item_index):
        if isinstance(item_index, slice):
            start, stop, step = item_index.indices(3)
            return [self.__array_rectangles[i] for i in range(start, stop, step)]

        if isinstance(item_index, int):
            if not self.__array_rectangles:
                return None

            return self.__array_rectangles[item_index]

    def free_space(self):
        return self.__free_space

    def add_rectangle(self, rect):
        """
        Adds a rectangle to the array if there is free space

        return: 'True' on success, 'False' if there is no free space
        """
        if not isinstance(rect, Rectangle):
            raise InitTypeError(type(rect).__name__)

        if self.__free_space > 0:
            self.__array_rectangles.append(rect)
            self.__update_array_stats(rect, self.__next_free_index)

            self.__free_space -= 1

            if not self.__free_space:
                self.__next_free_index = None
            else:
                self.__next_free_index += 1

            return True

        return False

    def number_max_area(self):
        """Return order number (index) of the rectangle with the maximum area value"""
        if self.__next_free_index is not None and self.__next_free_index < 1:
            return None

        return self.__number_max_area

    def number_min_perimeter(self):
        """Return order number(index) of the rectangle with the minimum area value"""
        if self.__next_free_index is not None and self.__next_free_index < 1:
            return None

        return self.__number_min_perimeter

    def number_square(self):
        """Return the number of squares in the array of rectangles"""
        if self.__next_free_index is not None and self.__next_free_index < 1:
            return None

        return self.__number_square


if __name__ == "__main__":
    driver.rectangle_usage_example(Rectangle)
    driver.init_empty_array(Rectangle, ArrayRectangles)
    driver.init_empty_array_with_max_length(Rectangle, ArrayRectangles)
    driver.init_array_with_arbitary_amount_of_rectangles(Rectangle, ArrayRectangles)
    driver.init_array_with_list(Rectangle, ArrayRectangles)
