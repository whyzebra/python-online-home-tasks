class Employee:
    def __init__(self, last_name, salary):
        self.__name = last_name
        self.__salary = salary
        self.__bonus = 0

    @property
    def name(self):
        return self.__name

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, salary):
        self.__salary = salary

    def set_bonus(self, bonus):
        self.__bonus = bonus

    def get_bonus(self):
        return self.__bonus

    # def get_salary(self):
    #     return self.__salary

    def to_pay(self):
        return self.__salary + self.__bonus


class SalesPerson(Employee):
    def __init__(self, last_name, salary, percent=100):
        super().__init__(last_name, salary)
        self.__percent = percent

    def set_bonus(self, bonus):
        if self.__percent > 200:
            bonus *= 3
        elif self.__percent > 100:
            bonus *= 2

        super().set_bonus(bonus)


class Manager(Employee):
    def __init__(self, last_name, salary, client_amount=50):
        super().__init__(last_name, salary)

        self.__quantity = client_amount

    def set_bonus(self, bonus):
        if self.__quantity > 150:
            bonus += 1000
        elif self.__quantity > 100:
            bonus += 500

        super().set_bonus(bonus)


class Company:
    def __init__(self, employees):
        self.__employees = []
        self.__employe_with_max_earnings = None

        if not isinstance(employees, list):
            raise TypeError(f'employees must be type \'list\', not {type(employees)}')

        if len(employees) < 1:
            raise ValueError('employees list must contain at least one \'Employee\'')

        max_earnings = 0
        for employee in employees:
            if not isinstance(employee, Employee):
                raise TypeError(f'each element in the list must be type \'Employee\', not {type(employee)}')

            self.__employees.append(employee)

            employe_earnings = employee.to_pay()
            if employe_earnings > max_earnings:
                max_earnings = employe_earnings
                self.__employe_with_max_earnings = employee

    def give_everybody_bonus(self, company_bonus):
        for employee in self.__employees:
            employee.set_bonus(company_bonus)

    def total_to_pay(self):
        total_to_pay = 0

        for employee in self.__employees:
            total_to_pay += employee.to_pay()

        return total_to_pay

    def name_max_salary(self):
        return self.__employe_with_max_earnings.name


def print_employe(employee, bonus=200):
    print(f'Employee name: {employee.name}')
    print(f'{employee.name} is: {employee.__class__.__name__}')
    print(f'{employee.name}`s salary is: {employee.salary}')
    print(f'Set {employee.name}`s bonus as: {bonus}')
    employee.set_bonus(bonus)
    print(f'Now {employee.name}`s bonus is: {employee.get_bonus()}')
    print(f'Sum to pay to {employee.name}: {employee.to_pay()}\n')


if __name__ == "__main__":
    print("alex = SalesPerson('Alex', salary=3500, percent=200)")
    alex = SalesPerson('Alex', salary=3500, percent=200)
    print_employe(alex, 200)
    # print(f'Employee name: {alex.name}')
    # print(f'Alex is: {alex.__class__.__name__}')
    # print(f'Alex`s salary is: {alex.salary}')
    # print('Set Alex`s bonus as: 1500')
    # alex.set_bonus(1500)
    # print(f'Now Alex`s bonus is: {alex.get_bonus()}')
    # print(f'Sum to pay to Alex: {alex.to_pay()}\n')

    print("jack = Manager('Jack', salary=5000, client_amount=250)")
    jack = Manager('Jack', salary=5000, client_amount=250)
    print(f'Employee name: {jack.name}')
    print(f'Jack is: {jack.__class__.__name__}')
    print(f'Jack`s salary is: {jack.salary}')
    print('Set Jack`s bonus as: 2000')
    jack.set_bonus(2000)
    print(f'Now Jack`s bonus is: {jack.get_bonus()}')
    print(f'Sum to pay to Jack: {jack.to_pay()}\n')

    print('company = Company([alex, jack])')
    company = Company([alex, jack])
    print('company.give_everybody_bonus(5)')
    company.give_everybody_bonus(5)
    print(f'Now Alex`s bonus is: {alex.get_bonus()}')
    print(f'Now Jack`s bonus is: {jack.get_bonus()}')
    print(f'Total to pay: {company.total_to_pay()}')
    print(f'{company.name_max_salary()} has max salary')
