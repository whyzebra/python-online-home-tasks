# **Python Home Task 7**

### **Exercise 1 (Rectangle):**
Develop `Rectangle` class with a predefined functionality.
On a Low level it is obligatory:
To develop Rectangle class with following content:
- 2 closed float sideA and sideB (sides A and B of the rectangle)
   Constructor with two parameters a and b (parameters specify rectangle sides)
   Constructor with a parameter a (parameter specify side A of a rectangle, side B is always equal to 5)
- Method `GetSideA`, returning value of the side A
- Method `GetSideB`, returning value of the side B
- Method `Area`, calculating and returning the area value
- Method `Perimeter`, calculating and returning the perimeter value
- Method `IsSquare`, checking whether current rectangle is shape square or not. Returns true if the shape is square and false in another case.
- Method `ReplaceSides`, swapping rectangle sides
### Solution:
```python
import sys
import array_rectangles_driver as driver


class InitValueError(Exception):
    """
    Exception raised during initializing ArrayRectangles with wrong `int` argument

    Attributes:
        max_length -- `int` argument (free_space) which value caused
        the error during `ArrayRectangles` initialization
        message -- explanation of the error
    """

    def __init__(self, max_length, message="ArrayRectangles must be initialized with \'int\' > 0"):
        self.max_length = max_length
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.max_length} -> {self.message}'


class InitTypeError(Exception):
    """
    Exception raised during initializing ArrayRectangles with objects that are not instance of `Rectangle`

    Attributes:
        wrong_type -- type of object that caused the error during `ArrayRectangles` initialization
        message -- explanation of the error
    """

    def __init__(self, wrong_type, message="all elements of an ArrayRectangles must be \'Rectangle\'"):
        self.wrong_type = wrong_type
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f'{self.wrong_type} -> {self.message}'


class Rectangle:
    def __init__(self, A, B=5.0):
        self.__side_A = float(A)
        self.__side_B = float(B)

    def __repr__(self):
        return f'Rectangle({self.__side_A}, {self.__side_B})'

    def get_side_A(self):
        return self.__side_A

    def get_side_B(self):
        return self.__side_B

    def area(self):
        return self.__side_A * self.__side_B

    def perimeter(self):
        return self.__side_A + self.__side_B

    def is_square(self):
        return self.__side_A == self.__side_B

    def replace_sides(self):
        self.__side_A, self.__side_B = self.__side_B, self.__side_A


class ArrayRectangles:
    def __init__(self, *args):
        self.__array_rectangles = []
        self.__free_space = 0
        self.__next_free_index = None

        self.__max_area = 0
        self.__number_max_area = 0

        self.__min_perimeter = sys.maxsize
        self.__number_min_perimeter = 0

        self.__number_square = 0

        if not args:
            self.__init_empty_array(array_length=5)

        if len(args) == 1:
            if not isinstance(args[0], (int, list, Rectangle)):
                raise InitTypeError(type(args[0]).__name__)

            if isinstance(args[0], int):
                if args[0] <= 0:
                    raise InitValueError(args[0])

                self.__init_empty_array(array_length=args[0])

            elif isinstance(args[0], list):
                self.__init_predefined_array(rectangles=args[0])

            elif isinstance(args[0], Rectangle):
                self.__init_predefined_array(rectangles=[args[0]])

        elif isinstance(args, tuple):
            self.__init_predefined_array(rectangles=args)

    def __init_empty_array(self, array_length):
        self.__free_space = array_length
        self.__next_free_index = 0

    def __init_predefined_array(self, rectangles):
        for rect_index, rect in enumerate(rectangles):
            if not isinstance(rect, Rectangle):
                raise InitTypeError(type(rect).__name__)

            else:
                self.__array_rectangles.append(rect)
                self.__update_array_stats(rect, rect_index)

    def __update_array_stats(self, rect, rect_index):
        if rect.is_square():
            self.__number_square += 1

        if rect.perimeter() < self.__min_perimeter:
            self.__min_perimeter = rect.perimeter()
            self.__number_min_perimeter = rect_index

        if rect.area() > self.__max_area:
            self.__max_area = rect.area()
            self.__number_max_area = rect_index

    def __repr__(self):
        return str(self.__array_rectangles)

    def __iter__(self):
        for rect in self.__array_rectangles:
            yield rect

    def __getitem__(self, item_index):
        if isinstance(item_index, slice):
            start, stop, step = item_index.indices(3)
            return [self.__array_rectangles[i] for i in range(start, stop, step)]

        if isinstance(item_index, int):
            if not self.__array_rectangles:
                return None

            return self.__array_rectangles[item_index]

    def free_space(self):
        return self.__free_space

    def add_rectangle(self, rect):
        """
        Adds a rectangle to the array if there is free space

        return: 'True' on success, 'False' if there is no free space
        """
        if not isinstance(rect, Rectangle):
            raise InitTypeError(type(rect).__name__)

        if self.__free_space > 0:
            self.__array_rectangles.append(rect)
            self.__update_array_stats(rect, self.__next_free_index)

            self.__free_space -= 1

            if not self.__free_space:
                self.__next_free_index = None
            else:
                self.__next_free_index += 1

            return True

        return False

    def number_max_area(self):
        """Return order number (index) of the rectangle with the maximum area value"""
        if self.__next_free_index is not None and self.__next_free_index < 1:
            return None

        return self.__number_max_area

    def number_min_perimeter(self):
        """Return order number(index) of the rectangle with the minimum area value"""
        if self.__next_free_index is not None and self.__next_free_index < 1:
            return None

        return self.__number_min_perimeter

    def number_square(self):
        """Return the number of squares in the array of rectangles"""
        if self.__next_free_index is not None and self.__next_free_index < 1:
            return None

        return self.__number_square


if __name__ == "__main__":
    driver.rectangle_usage_example(Rectangle)
    driver.init_empty_array(Rectangle, ArrayRectangles)
    driver.init_empty_array_with_max_length(Rectangle, ArrayRectangles)
    driver.init_array_with_arbitary_amount_of_rectangles(Rectangle, ArrayRectangles)
    driver.init_array_with_list(Rectangle, ArrayRectangles)
```
### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW7$ python3 array_rectangles.py 

    ==========================================
    === Example of 'Rectangle' class usage ===
    ==========================================

    rect = Rectangle(5, 10)
    rect.get_side_A() -> 5.0
    rect.get_side_B() -> 10.0
    rect.replace_sides()
    rect.get_side_A() -> 10.0
    rect.get_side_B() -> 5.0
    rect.perimeter()  -> 15.0
    rect.area()	  -> 50.0
    rect.is_square()  -> False

    =============================================
    === Init an empty 'ArrayRectangles' class ===
    ---------------------------------------------

    rectangles = ArrayRectangles()
    rectangles: []
    rectangles.number_max_area()      -> None
    rectangles.number_min_perimeter() -> None
    rectangles.number_square()        -> None
    free space -> 5

    rectangles.add_rectangle(Rectangle(20, 35)) -> True
    rectangles: [Rectangle(20.0, 35.0)]
    rectangles.number_max_area()      -> 0	| Rectangle(20.0, 35.0) at index 0 has max area = 700.0
    rectangles.number_min_perimeter() -> 0	| Rectangle(20.0, 35.0) at index 0 has min perimeter = 55.0
    rectangles.number_square()        -> 0	| There is 0 square in the rectangles array
    free space -> 4

    rectangles.add_rectangle(Rectangle(15, 15)) -> True
    rectangles: [Rectangle(20.0, 35.0), Rectangle(15.0, 15.0)]
    rectangles.number_max_area()      -> 0	| Rectangle(20.0, 35.0) at index 0 has max area = 700.0
    rectangles.number_min_perimeter() -> 1	| Rectangle(15.0, 15.0) at index 1 has min perimeter = 30.0
    rectangles.number_square()        -> 1	| There is 1 square in the rectangles array
    free space -> 3

    rectangles.add_rectangle(Rectangle(123, 456)) -> True
    rectangles: [Rectangle(20.0, 35.0), Rectangle(15.0, 15.0), Rectangle(123.0, 456.0)]
    rectangles.number_max_area()      -> 2	| Rectangle(123.0, 456.0) at index 2 has max area = 56088.0
    rectangles.number_min_perimeter() -> 1	| Rectangle(15.0, 15.0) at index 1 has min perimeter = 30.0
    rectangles.number_square()        -> 1	| There is 1 square in the rectangles array
    free space -> 2

    rectangles.add_rectangle(Rectangle(8, 2)) -> True
    rectangles: [Rectangle(20.0, 35.0), Rectangle(15.0, 15.0), Rectangle(123.0, 456.0), Rectangle(8.0, 2.0)]
    rectangles.number_max_area()      -> 2	| Rectangle(123.0, 456.0) at index 2 has max area = 56088.0
    rectangles.number_min_perimeter() -> 3	| Rectangle(8.0, 2.0) at index 3 has min perimeter = 10.0
    rectangles.number_square()        -> 1	| There is 1 square in the rectangles array
    free space -> 1

    rectangles.add_rectangle(Rectangle(999, 999)) -> True
    rectangles: [Rectangle(20.0, 35.0), Rectangle(15.0, 15.0), Rectangle(123.0, 456.0), Rectangle(8.0, 2.0), Rectangle(999.0, 999.0)]
    rectangles.number_max_area()      -> 4	| Rectangle(999.0, 999.0) at index 4 has max area = 998001.0
    rectangles.number_min_perimeter() -> 3	| Rectangle(8.0, 2.0) at index 3 has min perimeter = 10.0
    rectangles.number_square()        -> 2	| There are 2 squares in the rectangles array
    free space -> 0

    rectangles.add_rectangle(Rectangle(1, 1)) -> False
    rectangles: [Rectangle(20.0, 35.0), Rectangle(15.0, 15.0), Rectangle(123.0, 456.0), Rectangle(8.0, 2.0), Rectangle(999.0, 999.0)]

    Slicing example: rectangles[0:2] -> [Rectangle(20.0, 35.0), Rectangle(15.0, 15.0)]

    =========================================================================
    === Init an empty 'ArrayRectangles' with length (free_space) -> 'int' ===
    =========================================================================

    rectangles = ArrayRectangles(1)
    rectangles: []
    rectangles.number_max_area()      -> None
    rectangles.number_min_perimeter() -> None
    rectangles.number_square()        -> None
    free space -> 1

    rectangles.add_rectangle(Rectangle(123, 1)) -> True
    rectangles: [Rectangle(123.0, 1.0)]
    rectangles.number_max_area()      -> 0	| Rectangle(123.0, 1.0) at index 0 has max area = 123.0
    rectangles.number_min_perimeter() -> 0	| Rectangle(123.0, 1.0) at index 0 has min perimeter = 124.0
    rectangles.number_square()        -> 0	| There is 0 square in the rectangles array
    free space -> 0

    rectangles.add_rectangle(Rectangle(100, 200)) -> False
    rectangles: [Rectangle(123.0, 1.0)]

    ==========================================================================
    === Init 'ArrayRectangles' with arbitary amount of 'Rectangle' objects ===
    --------------------------------------------------------------------------

    rectangles = ArrayRectangles(Rectangle(15, 25), Rectangle(20, 20), Rectangle(8, 12))
    rectangles: [Rectangle(15.0, 25.0), Rectangle(20.0, 20.0), Rectangle(8.0, 12.0)]
    rectangles.number_max_area()      -> 1	| Rectangle(20.0, 20.0) at index 1 has max area = 400.0
    rectangles.number_min_perimeter() -> 2	| Rectangle(8.0, 12.0) at index 2 has min perimeter = 20.0
    rectangles.number_square()        -> 1	| There is 1 square in the rectangles array
    free space -> 0

    rectangles.add_rectangle(Rectangle(100, 200)) -> False
    rectangles: [Rectangle(15.0, 25.0), Rectangle(20.0, 20.0), Rectangle(8.0, 12.0)]

    ===============================================================
    === Init 'ArrayRectangles' with list of 'Rectangle' objects ===
    ---------------------------------------------------------------

    rectangles = ArrayRectangles([Rectangle(1, 2), Rectangle(3, 3), Rectangle(3, 3)])
    rectangles: [Rectangle(1.0, 2.0), Rectangle(3.0, 3.0), Rectangle(3.0, 3.0)]
    rectangles.number_max_area()      -> 1	| Rectangle(3.0, 3.0) at index 1 has max area = 9.0
    rectangles.number_min_perimeter() -> 0	| Rectangle(1.0, 2.0) at index 0 has min perimeter = 3.0
    rectangles.number_square()        -> 2	| There are 2 squares in the rectangles array
    free space -> 0

    rectangles.add_rectangle(Rectangle(100, 200)) -> False
    rectangles: [Rectangle(1.0, 2.0), Rectangle(3.0, 3.0), Rectangle(3.0, 3.0)]
----

### **Exercise 2 (Employee, SalesPerson, Manager):**
To create classes Employee, SalesPerson, Manager and Company with predefined functionality.

Low level requires:
To create basic class Employee and declare following content:
- Three closed fields — text field `name` (employee last name), money fields - `salary` and `bonus`
- Public property `Name` for reading employee's last name
- Public property `Salary` for reading and recording salary field
- Constructor with parameters string `name` and money `salary` (last name and salary are set)
- Method `SetBonus` that sets bonuses to `salary`, amount of which is delegated/conveyed as `bonus`
- Method `ToPay` that returns the value of summarized `salary` and `bonus`.

To create class `SalesPerson` as class `Employee` inheritor and declare within it:
- Closed integer field `percent` (percent of sales targets plan performance/execution)
constructor with parameters: `name` — employee last name, `salary`, `percent` — percent of plan performance, first two of which are passed to basic class constructor
- Redefine method of parent class `SetBonus` in the following way: 
    -if the sales person completed the plan more than 100%, so his bonus is doubled (is multiplied by 2)
    - and if more than 200% - bonus is tripled (is multiplied by 3)

To create class Manager as Employee class inheritor, and declare with it:
- Closed integer field `quantity` (number of clients, who were served by the manager during a month)
- Constructor with parameters string `name` - employee last name, `salary` and integer `clientAmount` ~ number of served clients, first two of which are passed to basic class constructor.
- Redefine method of parent class `SetBonus` in the following way: 
    - if the manager served over 100 clients, his bonus is increased by 500
    - if more than 150 clients ~ by 1000.


### Solution:
```python
class Employee:
    def __init__(self, last_name, salary):
        self.__name = last_name
        self.__salary = salary
        self.__bonus = 0

    @property
    def name(self):
        return self.__name

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, salary):
        self.__salary = salary

    def set_bonus(self, bonus):
        self.__bonus = bonus

    def get_bonus(self):
        return self.__bonus

    def get_salary(self):
        return self.__salary

    def to_pay(self):
        return self.__salary + self.__bonus


class SalesPerson(Employee):
    def __init__(self, last_name, salary, percent=100):
        super().__init__(last_name, salary)
        self.__percent = percent

    def set_bonus(self, bonus):
        if self.__percent > 200:
            bonus *= 3
        elif self.__percent > 100:
            bonus *= 2

        super().set_bonus(bonus)


class Manager(Employee):
    def __init__(self, last_name, salary, quantity=50):
        super().__init__(last_name, salary)

        self.__quantity = quantity

    def set_bonus(self, bonus):
        if self.__quantity > 150:
            bonus += 1000
        elif self.__quantity > 100:
            bonus += 500

        super().set_bonus(bonus)


if __name__ == "__main__":
    alex = SalesPerson('Alex', 3500, 200)
    print(f'Employee name: {alex.name}')
    print(f'Alex is: {alex.__class__}')
    print(f'Alex`s salary is: {alex.get_salary()}')
    print('Set Alex`s bonus as: 1500')
    alex.set_bonus(1500)
    print(f'Now Alex`s bonus is: {alex.get_bonus()}')
    print(f'Sum to pay to Alex: {alex.to_pay()}\n')

    jack = Manager('Jack', 5000, 250)
    print(f'Employee name: {jack.name}')
    print(f'Jack is: {jack.__class__}')
    print(f'Jack`s salary is: {jack.get_salary()}')
    print('Set Jack`s bonus as: 2000')
    jack.set_bonus(2000)
    print(f'Now Jack`s bonus is: {jack.get_bonus()}')
    print(f'Sum to pay to Jack: {jack.to_pay()}')
```
### output:
    D:\python-online-home-tasks\HW7>python task2.py
    Employee name: Alex
    Alex is: <class '__main__.SalesPerson'>
    Alex`s salary is: 3500
    Set Alex`s bonus as: 1500
    Now Alex`s bonus is: 3000
    Sum to pay to Alex: 6500

    Employee name: Jack
    Jack is: <class '__main__.Manager'>
    Jack`s salary is: 5000
    Set Jack`s bonus as: 2000
    Now Jack`s bonus is: 3000
    Sum to pay to Jack: 8000