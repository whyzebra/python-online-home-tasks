def print_array_rectangles_stats(rectangles):
    stats = f'rectangles: {rectangles}\n'

    max_area_index = rectangles.number_max_area()
    rect_with_max_area = rectangles[max_area_index]
    max_area = rect_with_max_area.area()
    stats += f'rectangles.number_max_area() {4*" "} -> {max_area_index}\t| '
    stats += f'{rect_with_max_area} at index {max_area_index} has max area = {max_area}\n'

    min_perimeter_index = rectangles.number_min_perimeter()
    rect_with_min_perimeter = rectangles[min_perimeter_index]
    min_perimeter = rect_with_min_perimeter.perimeter()
    stats += f'rectangles.number_min_perimeter() -> {min_perimeter_index}\t| '
    stats += f'{rect_with_min_perimeter} at index {min_perimeter_index} has min perimeter = {min_perimeter}\n'

    number_square = rectangles.number_square()
    stats += f'rectangles.number_square() {6*" "} -> {number_square}\t| '
    if number_square <= 1:
        stats += f'There is {number_square} square in the rectangles array\n'
    else:
        stats += f'There are {number_square} squares in the rectangles array\n'

    stats += f'free space -> {rectangles.free_space()}\n'
    print(stats)


def print_empty_array(rectangles):
    stats = f'rectangles: {rectangles}\n'

    max_area_index = rectangles.number_max_area()
    stats += f'rectangles.number_max_area() {4*" "} -> {max_area_index}\n'

    min_perimeter_index = rectangles.number_min_perimeter()
    stats += f'rectangles.number_min_perimeter() -> {min_perimeter_index}\n'

    number_square = rectangles.number_square()
    stats += f'rectangles.number_square() {6*" "} -> {number_square}\n'

    stats += f'free space -> {rectangles.free_space()}\n'

    print(stats)


def rectangle_usage_example(Rectangle):
    print(f'\n{42*"="}\n=== Example of \'Rectangle\' class usage ===\n{42*"="}\n')
    rect = Rectangle(5, 10)
    print('rect = Rectangle(5, 10)')
    print(f'rect.get_side_A() -> {rect.get_side_A()}')
    print(f'rect.get_side_B() -> {rect.get_side_B()}')
    print('rect.replace_sides()')
    rect.replace_sides()
    print(f'rect.get_side_A() -> {rect.get_side_A()}')
    print(f'rect.get_side_B() -> {rect.get_side_B()}')
    print(f'rect.perimeter()  -> {rect.perimeter()}')
    print(f'rect.area()\t  -> {rect.area()}')
    print(f'rect.is_square()  -> {rect.is_square()}\n\n\n')

    print(f'\n{45*"="}\n=== Init an empty \'ArrayRectangles\' class ===\n{45*"-"}\n')


def init_empty_array(Rectangle, ArrayRectangles):
    print('rectangles = ArrayRectangles()')
    rectangles = ArrayRectangles()
    print_empty_array(rectangles)

    status = rectangles.add_rectangle(Rectangle(20, 35))
    print(f'rectangles.add_rectangle(Rectangle(20, 35)) -> {status}')
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(15, 15))
    print(f'rectangles.add_rectangle(Rectangle(15, 15)) -> {status}')
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(123, 456))
    print(f'rectangles.add_rectangle(Rectangle(123, 456)) -> {status}')
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(8, 2))
    print(f'rectangles.add_rectangle(Rectangle(8, 2)) -> {status}')
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(999, 999))
    print(f'rectangles.add_rectangle(Rectangle(999, 999)) -> {status}')
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(1, 1))
    print(f'rectangles.add_rectangle(Rectangle(1, 1)) -> {status}')
    print(f'rectangles: {rectangles}\n')

    print(f'Slicing example: rectangles[0:2] -> {rectangles[0:2]}\n\n\n')


def init_empty_array_with_max_length(Rectangle, ArrayRectangles):
    print(f'\n{73*"="}')
    print('=== Init an empty \'ArrayRectangles\' with length (free_space) -> \'int\' ===')
    print(f'{73*"="}\n')

    print('rectangles = ArrayRectangles(1)')
    rectangles = ArrayRectangles(1)
    print_empty_array(rectangles)

    status = rectangles.add_rectangle(Rectangle(123, 1))
    print(f'rectangles.add_rectangle(Rectangle(123, 1)) -> {status}')
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(100, 200))
    print(f'rectangles.add_rectangle(Rectangle(100, 200)) -> {status}')
    print(f'rectangles: {rectangles}\n\n\n')


def init_array_with_arbitary_amount_of_rectangles(Rectangle, ArrayRectangles):
    print(f'\n{74*"="}')
    print('=== Init \'ArrayRectangles\' with arbitary amount of \'Rectangle\' objects ===')
    print(f'{74*"-"}\n')

    print('rectangles = ArrayRectangles(Rectangle(15, 25), Rectangle(20, 20), Rectangle(8, 12))')
    rectangles = ArrayRectangles(Rectangle(15, 25), Rectangle(20, 20), Rectangle(8, 12))
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(100, 200))
    print(f'rectangles.add_rectangle(Rectangle(100, 200)) -> {status}')
    print(f'rectangles: {rectangles}\n\n\n')


def init_array_with_list(Rectangle, ArrayRectangles):
    print(f'\n{63*"="}\n=== Init \'ArrayRectangles\' with list of \'Rectangle\' objects ===\n{63*"-"}\n')

    print('rectangles = ArrayRectangles([Rectangle(1, 2), Rectangle(3, 3), Rectangle(3, 3))]')
    rectangles = ArrayRectangles([Rectangle(1, 2), Rectangle(3, 3), Rectangle(3, 3)])
    print_array_rectangles_stats(rectangles)

    status = rectangles.add_rectangle(Rectangle(100, 200))
    print(f'rectangles.add_rectangle(Rectangle(100, 200)) -> {status}')
    print(f'rectangles: {rectangles}\n')
