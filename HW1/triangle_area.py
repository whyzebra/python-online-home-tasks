import math


def triangle_exists(a, b, c):
    """
    Return True if triangle with sides 'a', 'b' and 'c' exists.
    Rises ValueError if triangle does not exist.
    """
    if any(side <= 0 for side in [a, b, c]):
        raise ValueError("Negative values are not allowed!")

    if (a + b <= c) or (a + c <= b) or (b + c <= a):
        raise ValueError(
            f"Triangle with sides '{a}', '{b}', '{c}' does not exsit!"
        )

    return True


def get_triangle_area(a, b, c, precision=4):
    """
    Calculates Triangle Area by 3 given sides (a, b, c)

    Keyword arguments:
    precision -- amount of digits after comma (default 4)
    """
    if triangle_exists(a, b, c):
        sp = (a+b+c) / 2
        triangle_area = math.sqrt(sp * (sp-a) * (sp-b) * (sp-c))

        return round(triangle_area, int(precision))


if __name__ == '__main__':
    triangle_area = get_triangle_area(4.5, 5.9, 9, precision=2)
    print(f"Triangle area is {triangle_area}")
