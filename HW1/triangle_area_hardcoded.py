import math

a, b, c = 4.5, 5.9, 9

# calculating semiperimeter of the triangle
sp = (a+b+c) / 2

# area of triangle formula
area = math.sqrt(sp * (sp-a) * (sp-b) * (sp-c))

# using `round()` to set amount of digits after comma (precision)
print(f"Triangle area is {round(area, 2)}")
