# **Python Home Task 1**

## **Task:**
- Calculate the area of triangle with sides a = 4.5, b = 5.9 and c = 9;
- Print calculated value with precision = 2.

## **Solution:**
For this task I've used 2 different approaches of calculating triangle area:
1. Starightforward solution with "hardcoded" formula for given values `a`, `b` and `c`
2. General solution which calculates any triangle area and is based on two methods for:
    - validating triangle existence;
    - calculating triangle area with set precision.  

### **Method#1:** Straightforward solution

```python
import math

a, b, c = 4.5, 5.9, 9

# calculating semiperimeter of the triangle
sp = (a+b+c) / 2

# area of triangle formula
area = math.sqrt(sp * (sp-a) * (sp-b) * (sp-c))

# using `round()` to set amount of digits after comma (precision)
print(f"Triangle area is {round(area, 2)}")
```
#### **output:**
    D:\python-online-home-tasks\HW1>python triangle_area_hardcoded.py
    Triangle area is 11.58
----

### **Method#2:** Solution based on two methods

```python
import math


def triangle_exists(a, b, c):
    """
    Return True if triangle with sides 'a', 'b' and 'c' exists.
    Rises ValueError if triangle does not exist.
    """
    if any(side <= 0 for side in [a, b, c]):
        raise ValueError("Negative values are not allowed!")

    if (a + b <= c) or (a + c <= b) or (b + c <= a):
        raise ValueError(
            f"Triangle with sides '{a}', '{b}', '{c}' does not exsit!"
        )

    return True


def get_triangle_area(a, b, c, precision=4):
    """
    Calculates Triangle Area by 3 given sides (a, b, c)

    Keyword arguments:
    precision -- amount of digits after comma (default 4)
    """
    if triangle_exists(a, b, c):
        sp = (a+b+c) / 2
        triangle_area = math.sqrt(sp * (sp-a) * (sp-b) * (sp-c))

        return round(triangle_area, int(precision))


if __name__ == '__main__':
    triangle_area = get_triangle_area(4.5, 5.9, 9, precision=2)
    print(f"Triangle area is {triangle_area}")

```
#### **output:** 
    D:\python-online-home-tasks\HW1>python triangle_area.py
    Triangle area is 11.58
