# **Python Home Task 5**

### **Exercise 1 (IsSorted):**
- Create function **IsSorted**, determining whether a given **array** of integer values of arbitary lenght is sorted in a given **order**  
(the order is set up by enum value **SortOrder**). Array and sort order are passed by parameters. Function does not change the array.

#### Solution:
For this task, I`ve implemented class SortOrder, inherited from enum.Enum, with two values, which represent sort order:
- ascending
- descending

***is_sorted*** function takes *list* and *SortOrder* as arguments.
To check whether given list is sorted or not, function iterates over list  
and compares current value with previous, if current value is lower than previous, it means that list is not sorted.  
Depending on list order, function "reverse" list, so that it can be used in *for loop*
#### code:
```python
from enum import Enum


class SortOrder(Enum):
    ascending = 'ascending'
    descending = 'descending'


def is_sorted(data: list, sort_order: SortOrder = SortOrder.ascending) -> bool:
    if not isinstance(sort_order, SortOrder):
        raise TypeError('sort_order must be an instance of SortOrder Enum')

    if sort_order is SortOrder.descending:
        data = data[::-1]

    for prev_index, value in enumerate(data[1::]):
        if value < data[prev_index]:
            return False

    return True


if __name__ == "__main__":
    asc_list = [1, 2, 3, 4, 5]
    desc_list = asc_list[::-1]
    mixed_list = [99, 435, 23, 777, 214]

    print(f'list: {asc_list}')
    print(f'sorted in ascending order: {is_sorted(asc_list, SortOrder.ascending)}\n')

    print(f'list: {desc_list}')
    print(f'sorted in descending order: {is_sorted(desc_list, SortOrder.descending)}\n')

    print(f'list: {asc_list}')
    print(f'sorted in descending order: {is_sorted(asc_list, SortOrder.descending)}\n')

    print(f'list: {mixed_list}')
    print(f'sorted in ascending order: {is_sorted(mixed_list, SortOrder.ascending)}')
```
#### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW5$ python3 is_sorted.py 
    list: [1, 2, 3, 4, 5]
    sorted in ascending order: True

    list: [5, 4, 3, 2, 1]
    sorted in descending order: True

    list: [1, 2, 3, 4, 5]
    sorted in descending order: False

    list: [99, 435, 23, 777, 214]
    sorted in ascending order: False

----

### **Exercise 2 (Transform):**
- Create function **Transform**, replacing the value of each element of an integer **array** with the sum of this element value and its index,  
only if the given **array** is sorted in the given **order** (the order is set up by enum value **SortOrder**. Array and sort order are passed by parameters.  
To check, if the array is sorted, the function **IsSorted** from the ***Exercice 1*** is called.

#### Solution:
For this task I`ve implemented two different functions:
- *def transform(data, sort_order)*
- *def transform_return(data, sort_order)*

Function *transform* takes list and sort order as arguments, and transforms given *data*.  
Function *transform_return* takes same 2 arguments, but does not change given data, it returns new transformed list instead.

#### code:
```python
from is_sorted import is_sorted, SortOrder


def transform(data, sort_order):
    """Modifies :data: if it is sorted in :sort_order:"""
    if is_sorted(data, sort_order):
        for index, value in enumerate(data):
            data[index] = value + index


def transform_return(data, sort_order):
    """
    Returnes transformed version of :data:
    if :data: is sorted in :sort_order

    Does not modify :data: itself!
    """
    if is_sorted(data, sort_order):
        return [el+i for i, el in enumerate(data)]

    return data


if __name__ == "__main__":
    data = [1, 2, 3, 4, 5]
    print(f'initial list: {data}')
    transform(data, SortOrder.ascending)
    print(f'list is sorted in ascending order, now list is equel to: {data}')
```
#### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW5$ python3 transform.py 
    initial list: [1, 2, 3, 4, 5]
    list is sorted in ascending order, now list is equel to: [1, 3, 5, 7, 9]

----

### **Exercise 3 (MultArithmeticElements):**
- Create function **MultArithmeticElements**, which determines the multiplication of the first **n** elements of arithmetic progression  
of real numbers with a given initial element if progression **a<sub>1</sub>** and progression step **t**. **a<sub>n</sub>** is calculated by the formula *a<sub>n+1</sub> = a<sub>n</sub> + t*
#### Solution:
To solve this exercise I`ve implemented two functions:
- generator function *arithmetic_progression(start, step, n)*,which generates arithmetic progression elements sequence, it takes three argumetns:
    1. start: first element of progression
    1. step: aithemtic prgoression step
    1. n: number of last element of progression
- function *mult_arithemtic_elements(start, step, n)* counts product of multiplication of all elements in Arithemitc progression.  
To count product of all elements in the list, which represents Arithmetic progression, I used *math.prod()* method.  
As alternative way to count product, I`ve commented line with *functools.reduce()* method

#### code:
```python
from functools import reduce
from math import prod


def arithmetic_progression(start, step, n):
    """
    Generator of first :n: elements of arithmetic progression
    """
    stop = start + step*(n - 1) + 1
    el = start

    while el <= stop:
        yield el
        el += step


def mult_arithemtic_elements(start: int, step: int, n: int) -> int:
    """
    Returns multiplication of first :n: elements of arithemtic progression
    """
    # using reduce as alternative to math.prod
    # return reduce((lambda a, b: a * b), progression)
    return prod(arithmetic_progression(start, step, n))


if __name__ == "__main__":
    # driver code, with pretty terminal output,
    # to show that function 'mult_arithemtic_elements' is operationable
    kwargs = {
        'start': 5,
        'step': 3,
        'n': 7
    }
    progression = list(map(str, arithmetic_progression(**kwargs)))

    print(kwargs)
    print(f'Arithemtic progression is: {progression}')
    print(f'Arithemtic progression multiplication: {"*".join(progression)} = ', end='')

    # function usage:
    result = mult_arithemtic_elements(**kwargs)
    print(result)
```
#### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW5$ python3 mult_arithmetic_elements.py
    {'start': 5, 'step': 3, 'n': 7}
    Arithemtic progression is: ['5', '8', '11', '14', '17', '20', '23']
    Arithemtic progression multiplication: 5*8*11*14*17*20*23 = 48171200

----

### **Exercise 4 (SumGeometricElements):**
- Create function **SumGeometricElements**, determining the sum of the first elements of a decreasing geometric progression of real numbers  
with a given initial element of a progression **a<sub>n</sub>** and a given progression step **t**, while the last element must be greater than a given **alim**.  
**a<sub>n</sub>** is calculated by the formula (a<sub>n+1</sub> = a<sub>n</sub> * t), 0 < t < 1.
#### Solution:
To solve this exercise I`ve implemented two functions:
- generator function *geometric_progression(start, ratio, lim)*,which generates geometric progression elements sequence, it takes three argumetns:
    1. start: first element of progression
    1. ratio: multiplier for the next progression element
    1. lim: Nth element limit, after which generator stops to generate next values
- function *sum_geometric_elements(start, ratio, lim* counts the sum of all elements in geometric progression.  
To count sum of all elements in the list, which represents geometric progression, I used built-in *sum()* method.  

#### code:
```python
def geometric_progression(start, ratio, lim):
    """
    Generates geometric progression in rnage [start :lim:)

    :start: - first element of progression
    :ratio: - multiplier for the next progression element
    :lim: - Nth element limit, after which generator stops to count next elements
    """
    current_value = start

    while current_value > lim:
        yield current_value
        current_value *= ratio


def sum_geometric_elements(start, ratio, lim):
    return sum(geometric_progression(start, ratio, lim))


if __name__ == "__main__":
    # driver code, with pretty terminal output,
    # to show that function 'sum_geometric_elements' is operationable
    kwargs = {
        'start': 100,
        'ratio': 0.5,
        'lim': 20
    }

    print(kwargs)
    progression = list(map(str, geometric_progression(**kwargs)))
    print(f'Geometric progression is: {progression}')
    print(f'Geometric progression sum: {" + ".join(progression)} = ', end='')
    print(sum_geometric_elements(**kwargs))
```
#### output:
    whyzebra@ubuntu:~/development/python-online-home-tasks/HW5$ python3 sum_geometric_elements.py 
    {'start': 100, 'ratio': 0.5, 'lim': 20}
    Geometric progression is: ['100', '50.0', '25.0']
    Geometric progression sum: 100 + 50.0 + 25.0 = 175.0
