from functools import reduce
from math import prod


def arithmetic_progression(start, step, n):
    """
    Generator of first :n: elements of arithmetic progression
    """
    stop = start + step*(n - 1) + 1
    el = start

    while el <= stop:
        yield el
        el += step


def mult_arithemtic_elements(start: int, step: int, n: int) -> int:
    """
    Returns multiplication of first :n: elements of arithemtic progression
    """
    # using reduce as alternative to math.prod
    # return reduce((lambda a, b: a * b), progression)
    return prod(arithmetic_progression(start, step, n))


if __name__ == "__main__":
    # driver code, with pretty terminal output,
    # to show that function 'mult_arithemtic_elements' is operationable
    kwargs = {
        'start': 5,
        'step': 3,
        'n': 7
    }
    progression = list(map(str, arithmetic_progression(**kwargs)))

    print(kwargs)
    print(f'Arithemtic progression is: {progression}')
    print(f'Arithemtic progression multiplication: {"*".join(progression)} = ', end='')

    # function usage:
    result = mult_arithemtic_elements(**kwargs)
    print(result)
