from is_sorted import is_sorted, SortOrder


def transform(data, sort_order):
    """Modifies :data: if it is sorted in :sort_order:"""
    if is_sorted(data, sort_order):
        for index, value in enumerate(data):
            data[index] = value + index


def transform_return(data, sort_order):
    """
    Returnes transformed version of :data:
    if :data: is sorted in :sort_order

    Does not modify :data: itself!
    """
    if is_sorted(data, sort_order):
        return [el+i for i, el in enumerate(data)]

    return data


if __name__ == "__main__":
    data = [1, 2, 3, 4, 5]
    print(f'initial list: {data}')
    transform(data, SortOrder.ascending)
    print(f'list is sorted in ascending order, now list is equel to: {data}')
