def geometric_progression(start, ratio, lim):
    """
    Generates geometric progression in rnage [start :lim:)

    :start: - first element of progression
    :ratio: - multiplier for the next progression element
    :lim: - Nth element limit, after which generator stops to count next elements
    """
    current_value = start

    while current_value > lim:
        yield current_value
        current_value *= ratio


def sum_geometric_elements(start, ratio, lim):
    return sum(geometric_progression(start, ratio, lim))


if __name__ == "__main__":
    # driver code, with pretty terminal output,
    # to show that function 'sum_geometric_elements' is operationable
    kwargs = {
        'start': 100,
        'ratio': 0.5,
        'lim': 20
    }

    print(kwargs)
    progression = list(map(str, geometric_progression(**kwargs)))
    print(f'Geometric progression is: {progression}')
    print(f'Geometric progression sum: {" + ".join(progression)} = ', end='')
    print(sum_geometric_elements(**kwargs))
