from enum import Enum


class SortOrder(Enum):
    ascending = 'ascending'
    descending = 'descending'


def is_sorted(data: list, sort_order: SortOrder = SortOrder.ascending) -> bool:
    if not isinstance(sort_order, SortOrder):
        raise TypeError('sort_order must be an instance of SortOrder Enum')

    if sort_order is SortOrder.descending:
        data = data[::-1]

    for prev_index, value in enumerate(data[1::]):
        if value < data[prev_index]:
            return False

    return True


if __name__ == "__main__":
    asc_list = [1, 2, 3, 4, 5]
    desc_list = asc_list[::-1]
    mixed_list = [99, 435, 23, 777, 214]

    print(f'list: {asc_list}')
    print(f'sorted in ascending order: {is_sorted(asc_list, SortOrder.ascending)}\n')

    print(f'list: {desc_list}')
    print(f'sorted in descending order: {is_sorted(desc_list, SortOrder.descending)}\n')

    print(f'list: {asc_list}')
    print(f'sorted in descending order: {is_sorted(asc_list, SortOrder.descending)}\n')

    print(f'list: {mixed_list}')
    print(f'sorted in ascending order: {is_sorted(mixed_list, SortOrder.ascending)}')
